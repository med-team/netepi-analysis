#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
#   Define and load Australian NH&MRC grant funding data
#   See http://www.nhmrc.gov.au/funding/dataset/rmis/index.htm

# Python standard modules
import os
import csv
import sys

# 3rd Party Modules
# http://www.pfdubois.com/numpy/
import Numeric, MA
from mx.DateTime import DateTime

# SOOM modules
from SOOMv0 import *
from SOOMv0.Sources.CSV import *

def make_nhmrc(options):
    label="Australian NH&MRC grant funding 2000-2007"
    nhmrc = makedataset("nhmrc", label=label)

    nhmrc.addcolumn("grant_id",label="Grant ID",coltype="identity",datatype=int)
    nhmrc.addcolumn("chief_investigator_a",label="Chief Investigator A",coltype="categorical",datatype=str)
    nhmrc.addcolumn("funding_group",label="Main Funding Group",coltype="categorical",datatype=str)
    nhmrc.addcolumn("grant_type",label="Grant Type",coltype="categorical",datatype=str)
    nhmrc.addcolumn("grant_subtype",label="Grant Subtype",coltype="categorical",datatype=str)
    nhmrc.addcolumn("scientific_title",label="Scientific Title",coltype="searchabletext",datatype=str)
    nhmrc.addcolumn("simplified_title",label="Simplified Title",coltype="searchabletext",datatype=str)
    nhmrc.addcolumn("admin_inst",label="Administering Institution",coltype="categorical",datatype=str)
    nhmrc.addcolumn("state",label="State/Territory",coltype="categorical",datatype=str)
    nhmrc.addcolumn("sector",label="Sector",coltype="categorical",datatype=str)
    nhmrc.addcolumn("research_area",label="Broad Research Area",coltype="categorical",datatype=str)
    nhmrc.addcolumn("rfcd_main",label="Field of Research - main category",coltype="categorical",datatype=str)
    nhmrc.addcolumn("rfcd",label="Field of Research",coltype="categorical",datatype=str)
    nhmrc.addcolumn("app_yr",label="Application year",coltype="ordinal",datatype=int)
    nhmrc.addcolumn("start_yr",label="Commencement year",coltype="ordinal",datatype=int)
    nhmrc.addcolumn("funding_total",label="Total funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_1994",label="1994 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_1995",label="1995 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_1996",label="1996 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_1997",label="1997 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_1998",label="1998 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_1999",label="1999 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2000",label="2000 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2001",label="2001 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2002",label="2002 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2003",label="2003 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2004",label="2004 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2005",label="2005 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2006",label="2006 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2007",label="2007 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2008",label="2008 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2009",label="2009 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2010",label="2010 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2011",label="2011 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2012",label="2012 funding (AUD)",coltype="scalar",datatype=float)
    nhmrc.addcolumn("funding_2013",label="2013 funding (AUD)",coltype="scalar",datatype=float)

    if options.verbose:
        print nhmrc
    return nhmrc

def nhmrc_source(filename):
    nhmrc_columns = [
            DataSourceColumn("grant_id",label="Grant ID",coltype="identity",ordinalpos=0),
            DataSourceColumn("chief_investigator_a",label="Chief Investigator A",coltype="categorical",ordinalpos=1),
            DataSourceColumn("funding_group",label="Main Funding Group",coltype="categorical",ordinalpos=2),
            DataSourceColumn("grant_type",label="Grant Type",coltype="categorical",ordinalpos=3),
            DataSourceColumn("grant_subtype",label="Grant Subtype",coltype="categorical",ordinalpos=4),
            DataSourceColumn("scientific_title",label="Scientific Title",coltype="searchabletext",ordinalpos=5),
            DataSourceColumn("simplified_title",label="Simplified Title",coltype="searchabletext",ordinalpos=6),
            DataSourceColumn("admin_inst",label="Administering Institution",coltype="categorical",ordinalpos=7),
            DataSourceColumn("state",label="State/Territory",coltype="categorical",ordinalpos=8),
            DataSourceColumn("sector",label="Sector",coltype="categorical",ordinalpos=9),
            DataSourceColumn("research_area",label="Broad Research Area",coltype="categorical",ordinalpos=10),
            DataSourceColumn("rfcd_main",label="Field of Research - main category",coltype="categorical",ordinalpos=11),
            DataSourceColumn("rfcd",label="Field of Research",coltype="categorical",ordinalpos=12),
            DataSourceColumn("app_yr",label="Application year",coltype="ordinal",ordinalpos=13),
            DataSourceColumn("start_yr",label="Commencement year",coltype="ordinal",ordinalpos=14),
            DataSourceColumn("funding_total",label="Total funding (AUD)",coltype="scalar",ordinalpos=15),
            DataSourceColumn("funding_1994",label="1994 funding (AUD)",coltype="scalar",ordinalpos=16),
            DataSourceColumn("funding_1995",label="1995 funding (AUD)",coltype="scalar",ordinalpos=17),
            DataSourceColumn("funding_1996",label="1996 funding (AUD)",coltype="scalar",ordinalpos=18),
            DataSourceColumn("funding_1997",label="1997 funding (AUD)",coltype="scalar",ordinalpos=19),
            DataSourceColumn("funding_1998",label="1998 funding (AUD)",coltype="scalar",ordinalpos=21),
            DataSourceColumn("funding_1999",label="1999 funding (AUD)",coltype="scalar",ordinalpos=22),
            DataSourceColumn("funding_2000",label="2000 funding (AUD)",coltype="scalar",ordinalpos=23),
            DataSourceColumn("funding_2001",label="2001 funding (AUD)",coltype="scalar",ordinalpos=24),
            DataSourceColumn("funding_2002",label="2002 funding (AUD)",coltype="scalar",ordinalpos=25),
            DataSourceColumn("funding_2003",label="2003 funding (AUD)",coltype="scalar",ordinalpos=26),
            DataSourceColumn("funding_2004",label="2004 funding (AUD)",coltype="scalar",ordinalpos=27),
            DataSourceColumn("funding_2005",label="2005 funding (AUD)",coltype="scalar",ordinalpos=28),
            DataSourceColumn("funding_2006",label="2006 funding (AUD)",coltype="scalar",ordinalpos=29),
            DataSourceColumn("funding_2007",label="2007 funding (AUD)",coltype="scalar",ordinalpos=30),
            DataSourceColumn("funding_2008",label="2008 funding (AUD)",coltype="scalar",ordinalpos=31),
            DataSourceColumn("funding_2009",label="2009 funding (AUD)",coltype="scalar",ordinalpos=32),
            DataSourceColumn("funding_2010",label="2010 funding (AUD)",coltype="scalar",ordinalpos=33),
            DataSourceColumn("funding_2011",label="2011 funding (AUD)",coltype="scalar",ordinalpos=34),
            DataSourceColumn("funding_2012",label="2012 funding (AUD)",coltype="scalar",ordinalpos=35),
            DataSourceColumn("funding_2013",label="2013 funding (AUD)",coltype="scalar",ordinalpos=36),
            DataSourceColumn("total",label="Total funding (AUD)",coltype="scalar",ordinalpos=37),
    ]

    return CSVDataSource("nhmrc_data", nhmrc_columns, filename=filename, header_rows=1,
                         label="NH&MRC funding 2000-2007")


def load_nhmrc(nhmrc, filename, options):
    filename = os.path.join(options.datadir, filename)
    nhmrc_src = nhmrc_source(filename)
    if options.verbose:
        print nhmrc
    nhmrc.initialise()
    nhmrc.loaddata(nhmrc_src,
                      chunkrows=options.chunkrows,
                      rowlimit=options.rowlimit)
    nhmrc.finalise()

def load(options):
    ds = make_nhmrc(options)
    load_nhmrc(ds, 'nhmrc_grantdata.csv.gz', options)
    ds.save()

