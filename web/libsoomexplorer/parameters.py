#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: parameters.py 3701 2009-02-26 05:56:34Z andrewm $
# $HeadURL: https://wwwepi4.health.nsw.gov.au/svn/netepi/Analysis/trunk/web/libsoomexplorer/parameters.py $

import SOOMv0

from libsoomexplorer.dsparams import DSParams

class Parameters:
    """
    A container for plot parameters

    It must be possible to pickle this and unpickle against a different
    version of the code, so keep things simple: all referenced data must
    be self-contained, accesses this data must cope if the attribute is
    missing, etc.
    """
    def __init__(self, dsname=None, plottype=None):
        self.dsparams = DSParams(dsname)
        self.plottype = plottype
        self.loaded_from = ''
        self.save_as = ''

    def set_default(self, attr, value):
        """Set an attribute if it doesn't already exist"""
        if not hasattr(self, attr):
            setattr(self, attr, value)

    def do_colset(self, ctx, op, field, index):
        colset = getattr(self, field)
        getattr(self, 'colset_' + op)(colset, int(index))

    def colset_add(self, colset, field_count):
        if field_count == 1:
            v = None
        else:
            v = [None] * field_count
        colset.append(v)

    def colset_del(self, colset, index):
        del colset[int(index)]

    def colset_up(self, colset, index):
        if index > 0:
            colset[index], colset[index-1] = colset[index-1], colset[index]

    def colset_dn(self, colset, index):
        if index <= len(colset) - 1:
            colset[index], colset[index+1] = colset[index+1], colset[index]

    def do_filter(self, ctx, op, field, *args):
        getattr(getattr(self, field), 'do_' + op)(ctx, *args)

    def has_changed(self, field):
        old_value = getattr(self, '_last_' + field, None)
        cur_value = getattr(self, field, None)
        setattr(self, '_last_' + field, cur_value)
        return old_value != cur_value

    # "sys" fields know whether they've been set by the user or
    # set to a default by the system.
    def is_sys(self, field):
        value = getattr(self, field, None)
        if not value: 
            return True
        return value.replace('\r', '') == getattr(self, '_sys_' + field, None)

    def set_sys(self, field, value):
        setattr(self, field, value)
        setattr(self, '_sys_' + field, value)
