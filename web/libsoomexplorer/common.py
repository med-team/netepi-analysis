#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: common.py 3673 2009-02-02 06:01:30Z andrewm $
# $Source: /usr/local/cvsroot/NSWDoH/SOOMv0/web/libsoomexplorer/common.py,v $

import SOOMv0
from time import time as _time

class UIError(Exception): pass

ConversionError = ValueError, TypeError

class Timer:
    def __init__(self):
        self.reset()

    def reset(self):
        self.last_t = None
        self.times = []
        self.last_name = None
        self.start_t = None

    def _flush(self, now):
        if self.last_name:
            self.times.append((self.last_name, now - self.last_t))
            self.last_name = None

    def __len__(self):
        self._flush(_time())
        return len(self.times)

    def __call__(self, name):
        now = _time()
        if self.start_t is None:
            self.start_t = now
        self._flush(now)
        self.last_name = name
        self.last_t = now

    def end(self):
        now = _time()
        self._flush(now)
        if self.start_t is not None:
            self.times.append(('TOTAL', now - self.start_t))
        try:
            return self.times
        finally:
            self.reset()

# The timer is global, but we only service one request at a time, so this is
# acceptable.
timer = Timer()
