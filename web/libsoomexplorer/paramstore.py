#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: paramstore.py 3675 2009-02-02 06:50:31Z andrewm $
# $HeadURL: https://wwwepi4.health.nsw.gov.au/svn/netepi/Analysis/trunk/web/libsoomexplorer/paramstore.py $

"""
Abstractions representing the stored parameter sets (analyses)
"""

# Standard Libraries
import os
import cPickle as pickle
import fcntl
import errno
import re
import time
try:
    set
except NameError:
    from sets import Set as set

# 3rd Party
from mx import DateTime

# SOOM
import SOOMv0

# Application
from libsoomexplorer.common import *
import config

class Analysis:
    def __init__(self, label, dsname):
        self.label = label
        self.dsname = dsname
        self.fn = None

    def get_dataset(self):
        class BadDS:
            def __init__(self, label):
                self.label = '??? %s' % label
                self.date_updated = DateTime.now()
        try:
            return SOOMv0.dsload(self.dsname)
        except Exception, e:
            return BadDS(str(e))

    def make_fn(self):
        fn = '%s_%s_%s' % (self.label[:30], time.time(), self.dsname)
        fn = re.sub('[^a-zA-Z0-9_]', '', fn)
        return fn

    def write(self, dir, params):
        if self.fn is None:
            self.fn = self.make_fn()
        f = open(os.path.join(dir, self.fn), 'wb')
        try:
            pickle.dump(params, f, -1)
        finally:
            f.close()


class Analyses:

    def __init__(self):
        self.dir = os.path.join(config.data_dir, 'analyses')
        self.index_fn = os.path.join(self.dir, '.index')

    def load_index(self):
        try:
            f = open(self.index_fn, 'rb')
        except IOError, (eno, estr):
            if eno != errno.ENOENT:
                raise
            return []
        try:
            fcntl.lockf(f, fcntl.LOCK_SH)
            return pickle.load(f)
        finally:
            f.close()

    def __len__(self):
        return len(self.load_index())

    def available(self):
        try:
            files = set(os.listdir(self.dir))
        except OSError, (eno, estr):
            if eno != errno.ENOENT:
                raise
            return []
        available = []
        for an in self.load_index():
            try:
                files.remove(an.fn)
            except KeyError:
                pass
            else:
                available.append((an.label, an))
        # XXX At this point, /files/ contains all the entries that don't appear
        # in the index - we could/should add these to the index? 
        available.sort()
        return [an for label, an in available]

    def update_index(self, cb):
        try:
            f = open(self.index_fn, 'r+b')
        except IOError, (eno, estr):
            if eno != errno.ENOENT:
                raise
            f = None
        try:
            if f is None:
                os.mkdir(self.dir)
                fd = os.open(self.index_fn, os.O_WRONLY|os.O_CREAT|os.O_EXCL, 0666)
                f = os.fdopen(fd, 'wb')
                fcntl.lockf(f, fcntl.LOCK_EX)
                index = []
            else:
                fcntl.lockf(f, fcntl.LOCK_EX)
                index = pickle.load(f)
                f.seek(0, 0)
            cb(index)
            pickle.dump(index, f, -1)
            f.truncate()
        finally:
            f.close()

    def save(self, params, dsname, label, overwrite=False):
        def _save(index):
            if overwrite:
                for an in index:
                    if an.dsname == dsname and an.label == label:
                        an.write(self.dir, params)
                        return
            an = Analysis(label, dsname)
            an.write(self.dir, params)
            index.append(an)
        self.update_index(_save)

    def load(self, fn):
        assert not os.path.dirname(fn)
        f = open(os.path.join(self.dir, fn))
        try:
            try:
                return pickle.load(f)
            except Exception, e:
                raise UIError('Unable to load this analysis: %s' % e)
        finally:
            f.close()

    def find(self, fn):
        for an in self.load_index():
            if an.fn == fn:
                return an
        return None

    def delete(self, fn):
        def _delete(index):
            for i, an in enumerate(index):
                if an.fn == fn:
                    del index[i]
                    break
        self.update_index(_delete)


analyses = Analyses()
