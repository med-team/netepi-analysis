#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: base.py 3673 2009-02-02 06:01:30Z andrewm $
# $HeadURL: https://wwwepi4.health.nsw.gov.au/svn/netepi/Analysis/trunk/web/libsoomexplorer/output/base.py $

import os
import sys
import tempfile
import csv

import config

class OutputError(Exception):
    pass


class _OutputFile(object):
    def __init__(self, fn, label):
        self.fn = fn
        self.label = label

    def delete(self):
        try:
            os.unlink(self.fn)
        except OSError:
            pass

    def url(self):
        return '/%s/dynamic/%s' % (config.appname, os.path.basename(self.fn))

    def exists(self):
        return os.path.exists(self.fn)


class DownloadBase:
    def __init__(self, file_name, content_type=None):
        self.file_name = file_name
        if content_type is None:
            content_type = 'application/unknown'
        self.content_type = content_type

    def set_headers(self, ctx):
        ctx.set_save_session(False)
        # IE will not download via SSL if caching is disabled. 
        # See: http://support.microsoft.com/?kbid=323308
        ctx.del_header('Cache-Control')
        ctx.del_header('Pragma')
        ctx.set_header('Content-Type', self.content_type)
        ctx.set_header('Content-Disposition', 
                    'attachment; filename="%s"' % self.file_name)


class Download(DownloadBase):
    def __init__(self, file_name, data=None,
                    content_type='application/unknown'):
        DownloadBase.__init__(self, file_name, content_type)
        self.data = []
        if data:
            self.data.append(data)

    def write(self, data):
        self.data.append(data)

    def send(self, ctx):
        self.set_headers(ctx)
        ctx.send_content(''.join(self.data))


class CSVdownload(DownloadBase):
    def __init__(self, rowgen, file_name, 
                 content_type='application/vnd.ms-excel'):
        DownloadBase.__init__(self, file_name, content_type)
        self.rowgen = rowgen

    def send(self, ctx):
        class ReqFile:
            def __init__(self, out_fn):
                self.write = out_fn
        self.set_headers(ctx)
        ctx.write_headers()
        csv.writer(ReqFile(ctx.request.write_content)).writerows(self.rowgen)


class OutputBase(object):

    def __init__(self):
        self._reset()

    def _reset(self):
        self._files = []
        self._download = None

    def clear(self):
        for of in self._files:
            of.delete()
        self._reset()

    def files(self):
        return [of for of in self._files if of.exists()]

    def have_files(self):
        return len(self.files()) > 0

    def tempfile(self, ext, label=None):
        f, path = tempfile.mkstemp('.' + ext.lower(), 'soom', 
                                   config.dynamic_target)
        os.close(f)
        of = _OutputFile(path, label)
        self._files.append(of)
        return of

    def output_names(self):
        return [of.url() for of in self._files]

    def select_page(self, page):
        pass

    def have_download(self):
        return self._download is not None

    def download(self, *a, **kw):
        self._download = Download(*a, **kw)

    def csv_download(self, *a, **kw):
        self._download = CSVdownload(*a, **kw)

    def send_download(self, ctx):
        if self._download is not None:
            self._download.send(ctx)


class NullOut(OutputBase):

    def clear(self):
        pass
