#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: workspace.py 3676 2009-02-03 03:32:48Z andrewm $
# $HeadURL: https://wwwepi4.health.nsw.gov.au/svn/netepi/Analysis/trunk/web/libsoomexplorer/workspace.py $

from time import time
import SOOMv0
from libsoomexplorer import plottypes
from libsoomexplorer.parameters import Parameters
from libsoomexplorer.paramstore import analyses
from libsoomexplorer.dsparams import DSParams
from libsoomexplorer.output.base import NullOut, OutputError
from libsoomexplorer.output.plot import ImageOut
from libsoomexplorer.output.table import TableOut, CrosstabOut, DatasetRowsOut
from libsoomexplorer.output.twobytwo import TwoByTwoOut


class Workspace:
    """
    Analysis workspace

    Main Attributes:
        output          Currently selected output style
        outputs         Available output styles (and output params)
        params          Analysis-type (plottype) parameters
        plottype        Currently selected analysis ("plottype")
    """

    def __init__(self):
        self.params =  Parameters()
        self.plottype = None
        self.outputs = {
            'image': ImageOut(),
            'table': TableOut(),
            'crosstab': CrosstabOut(),
            'dsrows': DatasetRowsOut(),
            'twobytwo': TwoByTwoOut(),
        }
        self.output = NullOut()

    def clear_params(self):
        self.params = Parameters(self.params.dsparams.dsname,
                                 self.params.plottype)
        self.plottype.set_default()

    def datasets(self, filter=None, orderby='label'):
        datasets = []
        for dsname in SOOMv0.soom.available_datasets():
            ds = SOOMv0.dsload(dsname)
            if filter is None or filter(ds, self):
                datasets.append((getattr(ds, orderby), ds))
        datasets.sort()
        return [ds for o, ds in datasets]

    def available_datasets(self, filter=None):
        return [(ds.name, ds.label) for ds in self.datasets(filter)]

    def get_dataset(self):
        return self.params.dsparams.get_dataset()

    def get_filtered_dataset(self):
        return self.params.dsparams.get_filtered_dataset()

    def available_plottypes(self):
        return [(p.name, p.label) for p in plottypes.plottypes]

    def set_dataset(self, dsname):
        self.params.dsparams.set_dsname(dsname)
        self.set_plottype()

    def set_plottype(self):
        if not self.params.plottype:
            self.params.plottype = plottypes.plottypes[0].name
        if self.plottype is None or self.plottype.name != self.params.plottype:
            for plottype in plottypes.plottypes:
                if self.params.plottype == plottype.name:
                    break
            else:
                raise ValueError('bad plottype %r' % self.params.plottype)
            self.output.clear()
            self.plottype = plottype(self)
            self.plottype.set_default()
            self.params.loaded_from = ''
            return True
        return False

    def get_condcolparams(self):
        return self.plottype.get_condcolparams(self)

    def set_outtype(self, outtype):
        self.output = self.outputs[outtype]

    def refresh(self):
        self.set_plottype()
        self.plottype.refresh()

    def go(self):
        start = time()
        self.output.clear()
        self.plottype.go()
        self.output.elapsed = time() - start

    def save(self):
        self.output.clear()
        overwrite=(self.params.save_as == self.params.loaded_from)
        analyses.save(self.params, self.params.dsparams.dsname, 
                      self.params.save_as, overwrite=overwrite)

    def load(self, fn):
        self.params = analyses.load(fn)
        self.params.loaded_from = self.params.save_as
        self.refresh()
