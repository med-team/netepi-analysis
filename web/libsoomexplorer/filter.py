#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: filter.py 3695 2009-02-11 02:29:17Z andrewm $
# $Source: /usr/local/cvsroot/NSWDoH/SOOMv0/web/libsoomexplorer/filter.py,v $

# Standard libraries
import time
import re
import copy

# eGenix mx.Tools, http://www.egenix.com/files/python/mxTools.html
import mx

# SOOM NSWDoH
import SOOMv0

# Application modules
from libsoomexplorer import colvals
from libsoomexplorer.undo import UndoMixin

class Node:

    def __init__(self):
        self.children = []

    def update_path(self, path='0'):
        self.path = path
        for i, child in enumerate(self.children):
            child.update_path('%s_%d' % (path, i))

    def height(self):
        return sum([child.height() for child in self.children]) or 1

    def find_node(self, path):
        """ Given a PATH, find a node """
        if path == self.path:
            return self
        for child in self.children:
            node = child.find_node(path)
            if node:
                return node

    def find_parent_node(self, child_node):
        """ Given a node, find the parent """
        if child_node in self.children:
            return self
        for child in self.children:
            node = child.find_parent_node(child_node)
            if node:
                return node


class ContainerNode(Node):

    def __init__(self, name, *children):
        Node.__init__(self)
        self.name = name
        self.children = list(children)

    def describe(self, dsname):
        return self.name

    def as_string(self):
        children_as_str = [child.as_string() 
                           for child in self.children
                           if child.is_complete()]
        if not children_as_str:
            return ''
        return '(%s)' % ((' %s ' % self.name).join(children_as_str))

    def is_complete(self):
        for child in self.children:
            if child.is_complete():
                return True
        return False


class LeafNode(Node):

    def __init__(self, colname, op, value):
        Node.__init__(self)
        self.colname = colname
        if value is None:
            if op == '==':
                op = 'is null'
            elif op == '!=':
                op = 'is not null'
        self.op = op
        self.value = value

    def describe(self, dsname):
        def fmt(v):
            return col.do_format(col.do_outtrans(v))

        if not self.colname:
            return '???'
        col = SOOMv0.dsload(dsname).get_column(self.colname)
        if type(self.value) is tuple:
            value = ', '.join([fmt(v) for v in self.value])
        else:
            value = fmt(self.value)
        if self.op == 'contains':
            value = "[[%s]]" % value
        elif self.op.startswith('is'):
            value = ''
        return '%s %s %s' % (col.label, self.op, value)

    def as_string(self):
        if not self.is_complete():
            return ''
        value = self.value
        op = self.op
        if type(value) is mx.DateTime.DateTimeType:
            value = 'date(%s,%s,%s)' % (value.year, value.month, value.day)
        elif type(value) is tuple:
            # So 1-tuple is correctly represented (no trailing comma)
            value = '(%s)' % (', '.join([repr(v) for v in value]))
        elif op == "contains":
            value = "[[%s]]" % value
        elif op == 'is null':
            op = '=='
            value = 'null'
        elif op == 'is not null':
            op = '!='
            value = 'null'
        else:
            value = repr(value)
        return '%s %s %s' % (self.colname, op, value)

    def is_complete(self):
        return self.colname and self.op

class FilterError(Exception): pass

ops = [
    ('Simple', (
        ('op_equal',            '==', 'equal'),
        ('op_not_equal',        '!=', 'not equal'),
        ('op_less_than',        '<', 'less than'),
        ('op_less_equal',       '<=', 'less than or equal'),
        ('op_greater_than',     '>', 'greater than'),
        ('op_greater_equal',    '>=', 'greater than or equal'),
        ('op_equal',            'is null', 'is null'),
        ('op_not_equal',        'is not null', 'is not null'),
    )),
    ('Regular Expression', (
        ('op_regexp',           '~', 'regexp'),
        ('op_not_regexp',       '!~', 'not regexp'),
    )),
    ('Prefix', (
        ('op_equal_col',        '==:', 'starts with'),
        ('op_not_equal_col',    '!=:', 'does not start with'),
        ('op_less_than_col',    '<:', 'less than starting'),
        ('op_less_equal_col',   '<=:', 'less than or equal starting'),
        ('op_greater_than_col', '>:', 'greater than starting'),
        ('op_greater_equal_col','>=:', 'greater than or equal starting'),
    )),
    ('Sets', (
        ('op_in',               'in', 'in'),
        ('op_not_in',           'notin', 'not in'),
        ('op_in_col',           'in:', 'prefix in'),
        ('op_not_in_col',       'notin:', 'prefix not in'),
    )),
    ('Free text', (
        ('op_contains',         'contains', 'contains'),
    )),
]


class ExprValueBase:
    markup = 'none'

    def __init__(self, name, value, multiple=False):
        pass

    def search(self, workspace):
        pass

    def as_pytypes(self, workspace):
        return None

    def pretty_value(self, workspace):
        return ''

    def __nonzero__(self):
        return False

    def show_search_box(self, workspace):
        return False


class ExprValueNull(ExprValueBase):
    pass


class ExprValue(ExprValueBase):

    markup = 'general'
    split_re = re.compile('[ ]*,[ ]*')

    def __init__(self, name, value, multiple=False):
        self.name = name
        self.value = value
        self.multiple = multiple

    def search(self, workspace):
        pass

    def as_pytypes(self, workspace):
        col = workspace.get_dataset()[self.name]
        if self.multiple:
            value = self.value
            if type(self.value) in (str, unicode):
                value = self.split_re.split(self.value.strip())
            elif self.value is None:
                value = []
            return tuple([colvals.to_datatype(col, v) for v in value])
        else:
            return colvals.to_datatype(col, self.value)

    def strval(self):
        if self.value is None:
            return ''
        if self.multiple:
            return ', '.join(self.value)
        else:
            return str(self.value)

    def pretty_value(self, workspace):
        col = workspace.get_dataset()[self.name]
        value = self.as_pytypes(workspace)
        if self.multiple:
            return ', '.join([colvals.shorten_trans(col, v) for v in value])
        else:
            return colvals.shorten_trans(col, value)

    def __repr__(self):
        return '%s(%r, %r, %r)' %\
            (self.__class__.__name__,
             self.name,
             self.value,
             self.multiple)


class ExprTextArea(ExprValue):

    markup = 'textarea'


class ExprValueDiscrete(colvals.ColValSelect, ExprValueBase):

    markup = 'discrete'

    def show_search_box(self, workspace):
        return self.cardinality_is_high(workspace)


class ExprValueDate(ExprValueBase):

    markup = 'date'

    def __init__(self, name, value, multiple=False):
        if not value:
            value = mx.DateTime.now()
        self.year = value.year
        self.month = value.month
        self.day = value.day

    def search(self, workspace):
        pass

    def as_pytypes(self, workspace):
        try:
            return mx.DateTime.DateTime(int(self.year), 
                                        int(self.month), 
                                        int(self.day))
        except mx.DateTime.Error, e:
            raise FilterError('date: %s' % e)

    def pretty_value(self, workspace):
        return '%s-%s-%s' % (self.year, self.month, self.day)

    def yearopt(self):
        return range(2050, 1900, -1)

    def monthopt(self):
        months = [(i, n) for i, n in mx.DateTime.Month.items()
                         if type(i) is int]
        months.sort()
        return months

    def dayopt(self):
        return range(1, 32)


class ExpressionEdit:
    type = 'expr'

    EDITCOL = 0
    EDITOP = 1
    EDITVALUE = 2

    modes = [
        (EDITCOL, 'Column'),
        (EDITOP, 'Expression'),
        (EDITVALUE, 'Value'),
    ]

    grouplabels = (
        'Discrete',
        'Scalar',
        'Date/Time',
        'Free text',
        'Other',
    )

    def __init__(self, dsname, node):
        self.dsname = dsname
        self.state = self.EDITCOL
        self.colname = node.colname
        self.op = node.op
        self.node = node
        self.__value_type = None
        self.set_value(node.value)

    def get_column(self):
        if self.colname:
            ds = SOOMv0.dsload(self.dsname)
            return ds.get_column(self.colname)

    def col_ops(self):
        if not self.colname:
            return ops
        col = self.get_column()
        col_ops = []
        for grouplabel, group in ops:
            group = [(op, label)
                     for method, op, label in group
                     if hasattr(col, method)]
            if group:
                col_ops.append((grouplabel, group))
        return col_ops

    def is_set_op(self):
        return self.op in ('in', 'in:', 'notin', 'notin:')

    def is_pattern_op(self):
        return self.op.endswith(':') or self.op.endswith('~')

    def next(self):
        if ((self.state == self.EDITCOL and self.colname)
            or (self.state == self.EDITOP and self.op)):
            self.state += 1
            self.set_value()

    def set_value(self, value = None):
        col = self.get_column()
        if (not self.colname or not self.op 
            or self.op in ('is null', 'is not null')):
            value_method = ExprValueNull
        elif col.is_searchabletext():
            value_method = ExprTextArea
        elif self.is_pattern_op():
            value_method = ExprValue
        elif col.is_datetimetype():
            value_method = ExprValueDate
        elif not col.is_discrete():
            value_method = ExprValue
        else:
            value_method = ExprValueDiscrete
        value_type = self.colname, value_method, self.is_set_op()
        if value_type != self.__value_type:
            self.value = value_method(self.colname, value, 
                                      multiple=self.is_set_op())
            self.__value_type = value_type

    def colgroups(self, filter = None):
        ds = SOOMv0.dsload(self.dsname)
        cols = [(col.label, col) for col in ds.get_columns()]
        cols.sort()
        colgroups = {}
        for label, col in cols:
            groups = []
            if col.is_datetimetype():
                groups.append('Date/Time')
            if col.is_discrete():
                groups.append('Discrete')
            if col.is_searchabletext():
                groups.append('Free text')
            if col.is_scalar():
                groups.append('Scalar')
            if not groups:
                groups.append('Other')
            for group in groups:
                colgroups.setdefault(group, []).append(col)
        return [(gl, [(col.name, col.label) for col in colgroups[gl]])
                for gl in self.grouplabels if gl in colgroups]

    def colname_select(self, filter = None):
        if filter is None:
            filter = True
        elif filter == 'datetime':
            filter = lambda c: c.is_datetimetype()
        elif filter == 'discrete':
            filter = lambda c: c.is_discrete() and not c.is_datetimetype() \
                    and not c.is_searchabletext()
        elif filter == 'text':
            filter = lambda c: c.is_searchabletext()
        elif filter == 'other':
            filter = lambda c: not c.is_discrete() and not c.is_datetimetype() \
                    and not c.is_searchabletext()
        else:
            raise ValueError('bad column filter value')
        ds = SOOMv0.dsload(self.dsname)
        cols = [(col.label, col.name)
                for col in ds.get_columns()
                if filter(col)]
        cols.sort()
        cols.insert(0, ('-- select --', ''))
        return [(name, label) for label, name in cols]

    def get_available_values(self):
        """
        For discrete columns, return a list of potential values
        """
        col = self.get_column()
        if not col.is_discrete():
            return
        values = [(v, str(col.do_outtrans(v))[:45]) 
                  for v in col.inverted.keys()]
        if col.is_ordered():
            values.sort()
        else:
            values = [(l, v) for v, l in values]
            values.sort()
            values = [(v, l) for l, v in values]
        return values

    def pretty_value(self, workspace):
        return self.value.pretty_value(workspace)

    def show_search_box(self, workspace):
        return (self.state == self.EDITVALUE 
                and self.value.show_search_box(workspace))


class FilterInfo:
    def __init__(self, filter):
        self.name = filter.name
        self.label = filter.label


class AndOrEdit:
    type = 'andor'

    def __init__(self, dsname, node):
        self.dsname = dsname
        self.node = node
        self.name = node.name


class Filter(UndoMixin):
    def __init__(self, dsname):
        UndoMixin.__init__(self)
        assert dsname
        self.dsname = dsname
        self.name = None
        self.label = None
        self.copy_buffer = None
        self.root = LeafNode(None, None, None)
        self.root.update_path()
        self.clear_edit()
        self.clear_undo()
        self.updatetime = time.time()

    def set_root(self, root):
        self._replace_node(None, root)

    def clear_edit(self):
        self.edit_expr = None
        self.edit_info = None
        self.edit_andor = None

    def node_is_selected(self, node):
        return ((self.edit_expr and self.edit_expr.node == node)
                or (self.edit_andor and self.edit_andor.node == node))

    def undo(self):
        self.clear_edit()
        UndoMixin.undo(self)

    def redo(self):
        self.clear_edit()
        UndoMixin.redo(self)

    def as_string(self):
        return self.root.as_string()

    def in_edit(self):
        return self.edit_expr or self.edit_andor or self.edit_info

    def loaded(self):
        try:
            self.copy_buffer
        except AttributeError:
            self.copy_buffer = None

    def _edit_node(self, node):
        self.clear_edit()
        if isinstance(node, ContainerNode):
            self.edit_andor = AndOrEdit(self.dsname, node)
        else:
            self.edit_expr = ExpressionEdit(self.dsname, node)

    # Basic operations - these should either be in complementary pairs, or be
    # involute themselves, so "undo/redo" works.
    #
    # AM - I'm not convinced it's safe saving node instances in 
    # the undo data, but it would be hard to do it any other way.

    def _replace_node(self, node, new_node):
        parent = self.root.find_parent_node(node)
        if parent is None:
            self._record_undo('_replace_node', None, self.root)
            self.root = new_node
        else:
            i = parent.children.index(node)
            parent.children[i] = new_node
            self._record_undo('_replace_node', new_node, node)
        self.root.update_path()
        self._edit_node(new_node)

    def _add_node(self, node, new_node, index=None):
        self._record_undo('_del_node', new_node)
        if node is None:
            self.root = new_node
        elif index is None:
            node.children.append(new_node)
        else:
            node.children.insert(index, new_node)
        self.root.update_path()
        self._edit_node(new_node)

    def _del_node(self, node):
        parent = self.root.find_parent_node(node)
        if parent is None:
            # root node
            self._replace_node(None, LeafNode(None, None, None))
        else:
            i = parent.children.index(node)
            if len(parent.children) == 2:
                # If parent left with only one child, we eliminate it.
                self._replace_node(parent, parent.children[i ^ 1])
            else:
                del parent.children[i]
                self._record_undo('_add_node', parent, node, i)
                self.root.update_path()
                self._edit_node(parent)

    def _set_expr(self, node, colname, op, value):
        self._record_undo('_set_expr', node, node.colname, node.op, node.value)
        node.colname = colname
        node.op = op
        node.value = value

    def _set_info(self, name, label):
        self._record_undo('_set_info', self.name, self.label)
        self.name = name
        if not label:
            label = self.name
        self.label = label

    def _set_andor(self, node, name):
        self._record_undo('_set_andor', node, node.name)
        node.name = name
        if not self.edit_info:
            self.start_edit_node(node.path)


    # Higher level operations
    def start_edit_node(self, path):
        node = self.root.find_node(path)
        if not node:
            raise LookupError('node path %s not found' % path)
        self._edit_node(node)

    def add_expr(self, node):
        self._add_node(node, LeafNode(None, None, None))

    def del_node(self):
        edit = self.edit_andor or self.edit_expr
        assert edit is not None
        self._del_node(edit.node)
        self.copy_buffer = edit.node

    def splice_filter(self, filter):
        # Import another filter at /node/, replacing /node/
        edit = self.edit_andor or self.edit_expr
        assert edit is not None
        self.clear_edit()
        self._replace_node(edit.node, filter.root)

    def paste(self):
        if self.copy_buffer is not None:
            edit = self.edit_andor or self.edit_expr
            assert edit is not None
            self._replace_node(edit.node, copy.deepcopy(self.copy_buffer))

    def add_andor(self, node, name):
        # Insert at "node" a new and/or node, moving "node" down, and adding
        # a new leaf node.
        new_node = ContainerNode(name)
        new_node.children.append(node)
        new_expr_node = LeafNode(None, None, None)
        new_node.children.append(new_expr_node)
        self._replace_node(node, new_node)
        self._edit_node(new_expr_node)

    def expr_mode(self, mode):
        self.edit_expr.state = mode
        self.edit_expr.set_value()

    def expr_next(self):
        if self.edit_expr:
            self.edit_expr.next()

    def commit_edit_node(self, workspace):
        self._set_expr(self.edit_expr.node, self.edit_expr.colname,
                        self.edit_expr.op, 
                        self.edit_expr.value.as_pytypes(workspace))
        self.clear_edit()

    def commit_add_edit_node(self, workspace):
        node = self.edit_expr.node
        parent = self.root.find_parent_node(node)
        self.commit_edit_node(workspace)
        if parent is None:
            self.add_andor(node, 'and')
        else:
            self.add_expr(parent)

    def start_edit_info(self):
        self.edit_info = FilterInfo(self)

    def apply_info(self):
        self._set_info(self.edit_info.name, self.edit_info.label)
        self.clear_edit()

    def set_andor(self, node, name):
        self._set_andor(node, name)
