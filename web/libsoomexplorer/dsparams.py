#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: dsparams.py 3703 2009-03-03 04:58:06Z andrewm $
# $Source: /usr/local/cvsroot/NSWDoH/SOOMv0/web/libsoomexplorer/dsparams.py,v $

import copy
import SOOMv0
from libsoomexplorer.common import *
from libsoomexplorer.filterstore import filterstore
from libsoomexplorer.filterparse import parse, FilterParseError

class DSParams:
    """
    Dataset-specific parameters (only filter at this time).
    """

    def __init__(self, dsname=None):
        self.dsname = dsname
        self.clear()

    def __getstate__(self):
        state = dict(self.__dict__)
        state['_DSParams__filtered_ds'] = None
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        if self.filter:
            self.filter.loaded()

    def set_dsname(self, dsname):
        if dsname is not None and self.dsname != dsname:
            self.dsname = dsname
            self.clear()

    def clear(self):
        self.filterexpr = None
        self.filterlabel = None
        self.filter = None
        self.filtername = None
        self.__filtered_ds = None

    def get_dataset(self):
        return SOOMv0.dsload(self.dsname)

    def get_label(self):
        ds = self.get_dataset()
        return ds.label or ds.name

    def have_filterexpr(self):
        # User supplied filterexpr (which doesn't match /filter/)?
        if (self.filterexpr and (self.filter is None
                           or self.filter.as_string() != self.filterexpr)):
            return True
        return False

    def filter_modified(self):
        return (self.filter is not None and self.filter.modified())

    def _clean_filterexpr(self):
        return self.filterexpr.replace('\n', ' ').replace('\r', '')

    def filter_args(self, kw):
        if self.filterexpr:
            kw['filterexpr'] = self._clean_filterexpr()
            if self.filterlabel:
                kw['filterlabel'] = self.filterlabel

    def available_filters(self):
        if not self.dsname:
            return []
        filter_list = [(f.label.lower(), f.name, f.label) 
                       for f in filterstore.available_filters(self.dsname)]
        filter_list.sort()
        if self.have_filterexpr() or self.filter_modified():
            filter_list.insert(0, ('', '', '%s<modified>' % self.filterlabel))
        return [nl[1:] for nl in filter_list]

    def use_filter(self, filter):
        self.filterexpr = filter.as_string()
        if filter.label:
            self.filterlabel = filter.label 
        else:
            label = self.filterexpr
            if len(label) > 60:
                label = label[:60] + ' ...'
            self.filterlabel = label 
        self.filter = filter
        if filter.modified():
            self.filtername = ''
        else:
            self.filtername = filter.name

    def save_filter(self, filter):
        filter.clear_edit()
        filterstore.update_filter(filter)
        self.use_filter(filter)

    def delete_filter(self, filter):
        if filter.name:
            filterstore.delete_filter(filter)
        self.clear()

    def get_filtered_dataset(self):
        if self.__filtered_ds is None:
            kw = {}
            self.filter_args(kw)
            self.__filtered_ds = self.get_dataset().filter(kwargs=kw)
        return self.__filtered_ds

    # page_process() methods
    def do_clear(self, ctx):
        self.clear()

    def do_new(self, ctx):
        if self.have_filterexpr() or self.filter_modified():
            ctx.msg('warn', 'New filter (select "Abandon" to return to '
                    'previously unsaved filter)')
        self.edit_filter = filterstore.new_filter(self.dsname)
        ctx.push_page('filter', self)

    def do_edit(self, ctx):
        # User supplied filterexpr (which doesn't match /filter/)? parse & edit
        if self.have_filterexpr():
            self.filterlabel = self.filtername = ''
            try:
                root = parse(self.get_dataset(), self.filterexpr)
            except FilterParseError, e:
                self.edit_filter = None
                raise UIError(str(e))
            else:
                self.edit_filter = filterstore.new_filter(self.dsname)
                self.edit_filter.set_root(root)
                if self.filter is not None:
                    self.edit_filter.name = self.filter.name
                    self.edit_filter.label = self.filter.label
        # User selected filtername? load & edit
        elif self.filtername:
            if self.filtername == self.filter.name:
                self.edit_filter = self.filter
            else:
                self.edit_filter = self.load_filter(self.filtername)
                if self.filter_modified():
                    ctx.msg('warn', 'Loaded %r (select "Abandon" to return to '
                        'previously unsaved filter)' % self.edit_filter.label)
                else:
                    ctx.msg('info', 'Loaded %r' % self.edit_filter.label)
        elif self.filter:
            # Edit previously edited filter
            self.edit_filter = copy.deepcopy(self.filter)
        else:
            # No other request - create new filter 
            self.edit_filter = filterstore.new_filter(self.dsname)
        ctx.push_page('filter', self)

    def load_filter(self, filtername):
        return filterstore.load_filter(self.dsname, filtername)

    def do_load(self, ctx):
        if self.filtername:
            if self.have_filterexpr() or self.filter_modified():
                ctx.msg('warn', 'Previously modified filter has been discarded')
            self.use_filter(self.load_filter(self.filtername))
        else:
            ctx.msg('warn', 'Not loading - modified filter is already active')
