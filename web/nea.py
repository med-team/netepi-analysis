#!/usr/bin/python
#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
#
# $Id: nea.py 3703 2009-03-03 04:58:06Z andrewm $
# $HeadURL: https://wwwepi4.health.nsw.gov.au/svn/netepi/Analysis/trunk/web/nea.py $

# Standard libraries
import sys, os
import copy
import traceback

# Albatross, http://www.object-craft.com.au/projects/albatross/
from albatross import SimpleApp, SimpleAppContext

def is_fcgi():
    # If there's a better way of detecting a FastCGI environment, I'd love to
    # hear it.
    import socket, errno
    try:
        s=socket.fromfd(sys.stdin.fileno(), socket.AF_INET,
                        socket.SOCK_STREAM)
    except socket.error:
        return False
    try:
        try:
            s.getpeername()
        except socket.error, (eno, errmsg):
            return eno == errno.ENOTCONN
    finally:
        s.close()

use_fcgi = is_fcgi()
if use_fcgi:
    from albatross.fcgiapp import Request, running
else:
    from albatross.cgiapp import Request

# SOOM, NSWDoH
import SOOMv0 

# Application modules
from libsoomexplorer.workspace import Workspace
from libsoomexplorer.paramstore import analyses
from libsoomexplorer.filter import ExpressionEdit, Filter, FilterError
from libsoomexplorer.undo import UndoError
from libsoomexplorer.output.base import OutputError
from libsoomexplorer.common import *

import config

app_dir = os.path.dirname(__file__)
page_dir = os.path.join(app_dir, 'pages')
sys.path.insert(0, app_dir)

catchable_errors = (SOOMv0.Error, UIError, OutputError)


class PageBase:
    def page_display(self, ctx):
        workspace = getattr(ctx.locals, 'workspace', None)
        if workspace and workspace.output.have_download():
            try:
                ctx.locals.workspace.output.send_download(ctx)
            except Exception:
                traceback.print_exc(None, sys.stderr)
        else:
            ctx.locals.title = ctx.locals.appname
            ctx.run_template(self.name + '.html')

    def dispatch(self, ctx, *objects):
        for field in ctx.request.field_names():
            if field.endswith('.x'):
                field = field[:-2]
            elif field.endswith('.y'):
                continue
            subfields = field.split('/')
            for o in objects:
                meth = getattr(o, 'do_' + subfields[0], None)
                if meth:
                    ctx.catchcall(meth, ctx, *subfields[1:])
                    return True
        return False

    def page_process(self, ctx):
        self.dispatch(ctx, self)


class StartPage(PageBase):
    name = 'start'

    def page_enter(self, ctx):
        if not analyses:
            ctx.set_page('newanalysis')
        ctx.locals.delete_an = None
        ctx.add_session_vars('delete_an')

    def page_leave(self, ctx):
        ctx.del_session_vars('delete_an')

    def do_new(self, ctx):
        ctx.set_page('newanalysis')

    def do_analyse(self, ctx, fn):
        ctx.locals.workspace = Workspace()
        ctx.locals.workspace.load(fn)
        ctx.add_session_vars('workspace')
        ctx.set_page('params')

    def do_delete(self, ctx, fn):
        ctx.locals.delete_an = analyses.find(fn)

    def do_delete_confirm(self, ctx):
        analyses.delete(ctx.locals.delete_an.fn)
        self.do_delete_cancel(ctx)

    def do_delete_cancel(self, ctx):
        ctx.locals.delete_an = None


class NewAnalysisPage(PageBase):
    name = 'newanalysis'

    def page_enter(self, ctx):
        ctx.locals.workspace = Workspace()
        ctx.add_session_vars('workspace')

    def do_analyse(self, ctx, dsname):
        ctx.locals.workspace.set_dataset(dsname)
        ctx.set_page('params')

    def do_about(self, ctx, dsname):
        ctx.push_page('explore', dsname)

    def do_show_prepared(self, ctx):
        ctx.set_page('start')


class ParamPage(PageBase):
    name = 'params'

    def page_display(self, ctx):
        workspace = ctx.locals.workspace
        label = workspace.params.dsparams.get_label()
        ctx.locals.title = 'Dataset %r - %s' % (label, ctx.locals.appname)
        ctx.locals.condcolparams = workspace.get_condcolparams()
        PageBase.page_display(self, ctx)

    def do_info(self, ctx, param):
        dsname = getattr(ctx.locals.workspace.params, param + 'params').dsname
        if dsname:
            ctx.push_page('explore', dsname)

    def do_back(self, ctx):
        ctx.set_page('start')

    def do_save(self, ctx):
        ctx.push_page('paramsave')

    def do_ok(self, ctx):
        ctx.locals.workspace.go()
        if ctx.locals.workspace.output.inline:
            ctx.push_page('result')

    def do_plottype_reset(self, ctx):
        ctx.locals.workspace.clear_params()

    def do_hide_params(self, ctx):
        ctx.locals.workspace.plottype.hide_params = True
        if ctx.locals.workspace.output.have_files() and ctx.locals.workspace.output.inline:
            ctx.push_page('result')

    def do_edit_condcolparams(self, ctx):
        if not len(ctx.locals.workspace.get_condcolparams()):
            raise UIError('No conditioning columns?')
        ctx.push_page('condcolparams')

    def do_edit_exposed(self, ctx):
        ctx.push_page('twobytwoparams', 
                      ctx.locals.workspace.params.exposure_params)

    def do_edit_outcome(self, ctx):
        ctx.push_page('twobytwoparams', 
                      ctx.locals.workspace.params.outcome_params)

    def page_process(self, ctx):
        timer('refresh')
        ctx.catchcall(ctx.locals.workspace.refresh)
        timer('dispatch')
        self.dispatch(ctx, self, ctx.locals.workspace.params)


class ParamSavePage(PageBase):
    name = 'paramsave'

    def page_enter(self, ctx):
        params = ctx.locals.workspace.params
        if params.loaded_from:
            params.save_as = params.loaded_from
        else:
            params.save_as = params.title

    def do_back(self, ctx):
        ctx.pop_page()

    def do_save(self, ctx):
        ctx.locals.workspace.save()
        ctx.pop_page()


class ResultPage(PageBase):
    name = 'result'

    def do_back(self, ctx):
        ctx.set_page('start')

    def do_show_params(self, ctx):
        ctx.locals.workspace.plottype.hide_params = False
        ctx.pop_page()

    def do_prev_page(self, ctx):
        ctx.locals.workspace.output.prev_page()

    def do_next_page(self, ctx):
        ctx.locals.workspace.output.next_page()

    def page_process(self, ctx):
        if not self.dispatch(ctx, self):
            page = getattr(ctx.locals, 'select_page', [])
            ctx.locals.workspace.output.select_page(page)
        ctx.locals.select_page = None


class FilterPage(PageBase):
    name = 'filter'
    colnamefields = 'dtcol', 'disccol', 'textcol', 'othercol'

    def page_enter(self, ctx, dsparams):
        ctx.locals.dsparams = dsparams
        ctx.add_session_vars('dsparams')
        filter = ctx.locals.dsparams.edit_filter
        if not filter.name:
            filter.start_edit_node(filter.root.path)
        ctx.locals.filter = filter
        ctx.add_session_vars('filter')
        for a in self.colnamefields:
            setattr(ctx.locals, a, '')
        ctx.add_session_vars(*self.colnamefields)
        ctx.locals.colvalselect = None
        ctx.add_session_vars('colvalselect')
        ctx.locals.delete_confirm = False
        ctx.add_session_vars('delete_confirm')
        ctx.locals.want_import = False
        ctx.add_session_vars('want_import')
        ctx.locals.confirm_quit = False
        ctx.add_session_vars('confirm_quit')

    def page_leave(self, ctx):
        ctx.locals.dsparams.edit_filter = None
        ctx.del_session_vars('dsparams')
        ctx.del_session_vars('filter')
        ctx.del_session_vars(*self.colnamefields)
        ctx.del_session_vars('colvalselect')
        ctx.del_session_vars('delete_confirm')
        ctx.del_session_vars('want_import')
        ctx.del_session_vars('confirm_quit')

    def page_display(self, ctx):
        filter = ctx.locals.filter
        workspace = ctx.locals.workspace
        if filter.edit_expr and hasattr(filter.edit_expr, 'colname'):
            ctx.locals.colvalselect = filter.edit_expr.value
            ctx.locals.search_result = ctx.locals.colvalselect.search(workspace)
            for a in self.colnamefields:
                setattr(ctx.locals, a, filter.edit_expr.colname)
        else:
            ctx.locals.colvalselect = None
            ctx.locals.search_result = []
        PageBase.page_display(self, ctx)

    def do_cancel(self, ctx):
        if ctx.locals.filter.modified():
            ctx.locals.confirm_quit = True
        else:
            ctx.pop_page()

    def do_cancel_confirm(self, ctx):
        ctx.pop_page()

    def do_cancel_cancel(self, ctx):
        ctx.locals.confirm_quit = False
        
    def do_delete(self, ctx):
        ctx.locals.delete_confirm = True

    def do_delete_confirm(self, ctx):
        try:
            ctx.locals.dsparams.delete_filter(ctx.locals.filter)
        finally:
            ctx.locals.delete_confirm = False
        ctx.pop_page()

    def do_delete_cancel(self, ctx):
        ctx.locals.delete_confirm = False

    def do_save(self, ctx):
        if ctx.locals.filter.edit_info:
            ctx.locals.filter.apply_info()
        if not ctx.locals.filter.name:
            ctx.msg('warn', 'Give the filter a name before saving')
            self.do_edit_info(ctx)
        elif not ctx.locals.filter.modified():
            ctx.msg('warn', 'Filter has not been modified.')
        else:
            ctx.locals.dsparams.save_filter(ctx.locals.filter)
            ctx.pop_page()

    def do_okay(self, ctx):
        if ctx.locals.filter.edit_info:
            ctx.locals.filter.apply_info()
        if ctx.locals.filter.edit_expr:
            ctx.locals.filter.commit_edit_node(ctx.locals.workspace)
        ctx.locals.dsparams.use_filter(ctx.locals.filter)
        if ctx.locals.filter.modified():
            ctx.msg('warn', 'Filter has been modified, but not saved.')
        ctx.pop_page()

    def do_undo(self, ctx):
        ctx.locals.filter.undo()

    def do_redo(self, ctx):
        ctx.locals.filter.redo()

    def do_edit_info(self, ctx):
        ctx.locals.filter.start_edit_info()

    def do_info_edit_apply(self, ctx):
        ctx.locals.filter.apply_info()

    def do_clear_edit(self, ctx):
        ctx.locals.filter.clear_edit()

    def do_andor_insert(self, ctx, op):
        ctx.locals.filter.add_andor(ctx.locals.filter.edit_andor.node, op)

    def do_andor_change(self, ctx, op):
        ctx.locals.filter.set_andor(ctx.locals.filter.edit_andor.node, op)

    def do_andor_add_expr(self, ctx):
        ctx.locals.filter.add_expr(ctx.locals.filter.edit_andor.node)

    def do_expr_insert(self, ctx, op):
        ctx.locals.filter.add_andor(ctx.locals.filter.edit_expr.node, op)

    def do_expr_select(self, ctx, mode):
        ctx.locals.filter.expr_mode(int(mode))

    def do_delete_node(self, ctx):
        ctx.locals.filter.del_node()

    def do_expr_okay(self, ctx):
        ctx.locals.filter.commit_edit_node(ctx.locals.workspace)

    def do_expr_okay_add(self, ctx):
        ctx.locals.filter.commit_add_edit_node(ctx.locals.workspace)

    def do_req_import(self, ctx):
        ctx.locals.want_import = True

    def do_paste(self, ctx):
        ctx.locals.filter.paste()

    def do_import_cancel(self, ctx):
        ctx.locals.want_import = False

    def do_import(self, ctx):
        try:
            filter = ctx.locals.dsparams.load_filter(ctx.locals.import_filter)
            ctx.locals.filter.splice_filter(filter)
        finally:
            ctx.locals.want_import = False

    def do_node(self, ctx, *args):
        ctx.locals.filter.start_edit_node(*args)

    def do_sop(self, ctx, *args):
        ctx.locals.colvalselect.sop(ctx.locals.workspace, *args)

    def page_process(self, ctx):
        if not self.dispatch(ctx, self):
            ctx.locals.filter.expr_next()


class DSMeta:
    def __init__(self, dsname):
        self.dsname = dsname
        self.show_col = None

    def get_dataset(self):
        return SOOMv0.dsload(self.dsname)
    
    def describe_ds(self):
        return self.get_dataset().describe(SOOMv0.SOME_DETAIL).describe_tuples()

    def describe_cols(self):
        return self.get_dataset().describe_cols()

    def describe_col(self, colname):
        return self.get_dataset()[colname].describe(SOOMv0.SOME_DETAIL).describe_tuples()


class ExplorePage(PageBase):
    name = 'explore'

    def page_enter(self, ctx, dsname):
        ctx.locals.dsmeta = DSMeta(dsname)
        ctx.add_session_vars('dsmeta')

    def page_leave(self, ctx):
        ctx.del_session_vars('dsmeta')

    def do_back(self, ctx):
        ctx.pop_page()

    def do_allcols(self, ctx):
        ctx.locals.dsmeta.show_col = None

    def do_view(self, ctx, show_col):
        ctx.locals.dsmeta.show_col = show_col


class CondColParamsPage(PageBase):
    name = 'condcolparams'

    def page_enter(self, ctx):
        workspace = ctx.locals.workspace
        ctx.locals.condcolparams = workspace.get_condcolparams()
        ctx.locals.colvalselect = None
        ctx.add_session_vars('condcolparams', 'colvalselect')

    def page_leave(self, ctx):
        ctx.del_session_vars('condcolparams', 'colvalselect')

    def page_display(self, ctx):
        condcolparams = ctx.locals.condcolparams
        workspace = ctx.locals.workspace
        # We want the search result to be transient
        ctx.locals.search_result = condcolparams.maybe_search(workspace)
        PageBase.page_display(self, ctx)

    def do_okay(self, ctx):
        ctx.pop_page()
        param_map = ctx.locals.condcolparams.get_map(ctx.locals.workspace)
        ctx.locals.workspace.params.condcolparams.update(param_map)

    def do_back(self, ctx):
        ctx.pop_page()

    def do_clear(self, ctx):
        ctx.locals.condcolparams.clear(ctx.locals.workspace)

    def do_edit_okay(self, ctx):
        ctx.locals.condcolparams.done_edit(ctx.locals.workspace)
        ctx.locals.colvalselect = None

    def do_col(self, ctx, *fields):
        ctx.locals.condcolparams.do_col(ctx.locals.workspace, *fields)
        ctx.locals.colvalselect = ctx.locals.condcolparams.edit_col.edit

    def do_sop(self, ctx, *fields):
        ctx.locals.colvalselect.sop(ctx.locals.workspace, *fields)


class TwoByTwoParamsPage(PageBase):
    name = 'twobytwoparams'

    def page_enter(self, ctx, params):
        params.save_undo()
        ctx.locals.params = params
        ctx.add_session_vars('params')

    def page_leave(self, ctx):
        ctx.del_session_vars('params')

    def do_back(self, ctx):
        ctx.locals.params.undo()
        ctx.pop_page()

    def do_okay(self, ctx):
        param_map = ctx.locals.params.get_map(ctx.locals.workspace)
        ctx.locals.workspace.params.condcolparams.update(param_map)
        ctx.locals.params.clear_undo()
        ctx.pop_page()

    def page_process(self, ctx):
        self.dispatch(ctx, self, ctx.locals.params)
        ctx.locals.params.search(ctx.locals.workspace)


class Context(SimpleAppContext):
    def __init__(self, app):
        SimpleAppContext.__init__(self, app)
        for attr in ('appname', 'apptitle'):
            setattr(self.locals, attr, getattr(config, attr))
            self.add_session_vars(attr)
        self.locals.soomversion = SOOMv0.version
        self.locals.msgs = []
        timer.reset()
        timer('init context')
        self.run_template_once('macros.html')

    def msg(self, lvl, msg):
        if lvl not in ('info', 'warn', 'err'):
            lvl = 'warn'
        self.locals.msgs.append((lvl, msg))

    def catchcall(self, fn, *a, **kw):
        try:
            fn(*a, **kw)
            return False
        except (SOOMv0.ExpressionError, SOOMv0.yappsrt.SyntaxError), e:
            self.msg('err', 'Filter: %s' % e)
        except (UIError, UndoError, FilterError), e:
            self.msg('err', e)
        except catchable_errors, e:
            self.msg('err', '%s: %s' % (e.__class__.__name__, e))
        return True


class Application(SimpleApp):
    pages = (
        CondColParamsPage,
        ExplorePage,
        FilterPage,
        NewAnalysisPage,
        ParamPage,
        ParamSavePage,
        ResultPage,
        StartPage,
        TwoByTwoParamsPage,
    )

    def __init__(self):
        SimpleApp.__init__(self,
                           base_url = 'nea.py',
                        #  module_path=page_dir,
                           template_path=page_dir,
                           start_page='start',
                           secret=config.session_secret)
        for page in self.pages:
            self.register_page(page.name, page())

    def create_context(self):
        return Context(self)


SOOMv0.soom.setpath(config.soompath, config.data_dir)
app = Application()

if __name__ == "__main__":
    import signal, rpy
    signal.signal(signal.SIGSEGV, signal.SIG_DFL)

    if not os.path.isabs(config.soompath):
        config.soompath = os.path.join(app_dir, config.soompath)

    if use_fcgi:
        while running():
            app.run(Request())
    else:
        app.run(Request())
