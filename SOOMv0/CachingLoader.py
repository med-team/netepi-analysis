#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#

# $Id: CachingLoader.py 2901 2007-11-20 04:52:21Z andrewm $
# $HeadURL$

# Standard Library
import os
import gzip
import errno
import array
import cPickle
from time import time

# Application modules
from common import *
from SOOMv0.Utils import quiet_unlink
from SOOMv0.Soom import soom
from ChunkingLoader import ChunkingLoader

class _CLState:
    def __init__(self, il):
        self.earliest = il.earliest
        self.latest = il.latest
        self.key_to_recno = il.key_to_recno
        self.colnames = [col.name for col, data in il.columns]
        self.numchunks = il.numchunks
        self.load_count = il.load_count
        assert len(il.recno_chunk) == len(il.recno_chunk_offs)
        self.rowcount = len(il.recno_chunk)

    def apply(self, il):
        il.earliest = self.earliest
        il.latest = self.latest
        il.key_to_recno = self.key_to_recno
        il.numchunks = self.numchunks
        il.load_count = self.load_count + 1
        il.rowcount = self.rowcount

class CachingLoader(ChunkingLoader):
    """
    The CachingLoader is an elaboration on the ChunkingLoader. It allows
    previously loaded data to be retained in the "chunks", with subsequent
    source data overloading the chunk data (based on the key_column). This
    means that the source only needs to supply rows that have been updated
    since the last load, rather than the full dataset. For slow sources
    (such as relational databases), this can be a big win.

    It works by retaining the column "chunks", with an index that maps from
    the datasource primary key to a "record number" (key_to_recno). Two arrays
    then map this to a chunk number (recno_chunk) and an offset within the
    chunk (recno_chunk_offs).
    """
    FN_MD = 'metadata.pkl.gz'
    FN_CM = 'recno_chunk.array'
    FN_CO = 'recno_chunk_offs.array'
    CHECKPOINT_INTERVAL = 600

    def __init__(self, columns, basepath, key_column, update_time_column=None):
        ChunkingLoader.__init__(self, columns, basepath)
        self.key_column = key_column
        self.update_time_column = update_time_column
        self._init()
        self.load()
        if not self.data_okay:
            self.clear()

    def _metadata_filename(self, name):
        return os.path.join(self.basepath, name)

    def _init(self):
        ChunkingLoader._init(self)
        self.key_to_recno = {}
        self.recno_chunk = array.array('L')
        self.recno_chunk_offs = array.array('L')
        self.earliest = None
        self.latest = None
        self.data_okay = False
        self.load_count = 1
        self.last_save = None

    def clear(self):
        self._init()
        quiet_unlink(self._metadata_filename(self.FN_MD))
        quiet_unlink(self._metadata_filename(self.FN_CM))
        quiet_unlink(self._metadata_filename(self.FN_CO))
        ChunkingLoader.clear(self)

    def _load_array(self, fn, n):
        fn = self._metadata_filename(fn)
        try:
            f = open(fn, 'rb')
        except IOError, (eno, estr):
            if eno == errno.ENOENT:
                return
            raise
        data = array.array('L')
        try:
            data.fromfile(f, n)
        finally:
            f.close()
        return data

    def _load_meta(self):
        fn = self._metadata_filename(self.FN_MD)
        try:
            f = gzip.open(fn, 'rb')
        except IOError, (eno, estr):
            if eno == errno.ENOENT:
                return None
            raise
        try:
            return cPickle.load(f)
        finally:
            f.close()

    def load(self):
        t0 = time()
        state = self._load_meta()
        if not state:
            return
        this_cols = set([col.name for col, data in self.columns])
        load_cols = set(state.colnames)
        if this_cols == load_cols:
            # Check that all existing chunks are available
            for colname in state.colnames:
                for chunknum in range(state.numchunks):
                    fn = self._chunk_filename(colname, chunknum)
                    if not os.access(fn, os.R_OK):
                        return
            t1 = time()
            recno_chunk = self._load_array(self.FN_CM, state.rowcount)
            recno_chunk_offs = self._load_array(self.FN_CO, state.rowcount)
            now = time()
            soom.info('Caching load, generation %s, %s keys, took %.1fs '
                      '(%.1fs metadata, %.1fs index)' % 
                      (state.load_count, len(state.key_to_recno),
                       now - t0, t1 - t0, now - t1))
            self.data_okay = True
            state.apply(self)
            self.recno_chunk = recno_chunk
            self.recno_chunk_offs = recno_chunk_offs

    def _save_array(self, fn, data):
        tmpfn = os.path.join(self.basepath, '.%s.tmp' % fn)
        f = open(tmpfn, 'wb')
        try:
            data.tofile(f)
            f.close()
            os.rename(tmpfn, self._metadata_filename(fn))
        except:
            f.close()
            os.unlink(tmpfn)

    def _save_meta(self):
        fn = self._metadata_filename(self.FN_MD)
        tmpfn = os.path.join(self.basepath, '.loader_state.tmp')
        f = gzip.open(tmpfn, 'wb')
        try:
            cPickle.dump(_CLState(self), f, -1)
            f.close()
            os.rename(tmpfn, fn)
        except:
            f.close()
            os.unlink(tmpfn)

    def save(self):
        t0 = time()
        self._save_array(self.FN_CM, self.recno_chunk)
        self._save_array(self.FN_CO, self.recno_chunk_offs)
        t1 = time()
        self._save_meta()
        t2 = time()
        el = t2 - t0
        soom.info('Save caching loader state took %.1fs (%.1fs array, %.1fs meta)' % (el, t1-t0, t2-t1))
        return el

    def flush(self):
        flush_el = ChunkingLoader.flush(self)
        now = time()
        if self.last_save + self.CHECKPOINT_INTERVAL < now:
            # Checkpoint
            flush_el += self.save()
            self.last_save = time()
        return flush_el

    def loadrows(self, sourcename, source, chunkrows=0, rowlimit=0):
        self.last_save = time()
        def _mapsource(source):
            for row in source:
                key = row.get(self.key_column)
                if self.update_time_column is not None:
                    update_time = row.get(self.update_time_column)
                    if self.earliest is None or update_time < self.earliest:
                        self.earliest = update_time
                    if self.latest is None or update_time > self.latest:
                        self.latest = update_time
                index = self.key_to_recno.get(key)
                if index is None:
                    self.key_to_recno[key] = len(self.recno_chunk)
                    self.recno_chunk.append(self.numchunks)
                    self.recno_chunk_offs.append(self.chunklen)
                else:
                    self.recno_chunk[index] = self.numchunks
                    self.recno_chunk_offs[index] = self.chunklen
                yield row
        return ChunkingLoader.loadrows(self, sourcename, _mapsource(source), 
                                       chunkrows, rowlimit)

    def load_completed(self):
        self.save()
        ChunkingLoader.load_completed(self)
        soom.info('Caching load yields %s rows (%s new) from %s generations' % 
                  (len(self.key_to_recno), self.rownum, self.load_count))
        return len(self.key_to_recno)

    def unchunk_columns(self):
        t0 = time()
        soom.mem_report()
        chunk_offs = zip(self.recno_chunk, self.recno_chunk_offs)
        chunk_offs.sort()
        soom.info('Caching unchunk sort took %.1fs' % (time() - t0))
        soom.mem_report()
        def _col_generator(colname):
            last_chunknum = -1
            data = None
            for chunknum, offset in chunk_offs:
                if chunknum != last_chunknum:
                    data = self.get_chunk(colname, chunknum)
                    last_chunknum = chunknum
                yield data[offset]

        for col, data in self.columns:
            yield col, _col_generator(col.name)
