#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: SummaryCond.py 2859 2007-10-18 07:45:37Z andrewm $

# Standard Library
import sys
import time
import bisect
# 3rd Party
import MA, Numeric
# Application
from SOOMv0.common import *
from SOOMv0.Soom import soom
import soomfunc

__all__ = (
    'numcmp', 
    'suppress', 'retain', 'order', 'reversed', 'coalesce', 'bins', 
    'condcol',
)

def numcmp(a, b):
    import math
    try:
        a = float(a)
        b = float(b)
    except ValueError:
        return 0
    else:
        return int(math.ceil(a - b))


class CondColArg(object):
    """
    Subclasses of this abstract class contain parameters that are
    later applied to the summary conditioning columns. Instances of
    these classes may be pickled or shared between summary calls,
    so members should be considered R/O.
    """
    def apply(self, summcondcol):
        raise NotImplementedError

    def usage(self, msg):
        raise Error('%s: %s' % (self.__class__.__name__, msg))

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__,
                           ', '.join([repr(v) for v in self.values]))


class CondColArgValues(CondColArg):

    def __init__(self, *values):
        self.callable = None
        if values:
            if callable(values[0]):
                if len(values) > 1:
                    self.usage('only one callable argument')
                self.callable = values[0]
                if isinstance(self.callable, type):
                    # Instantiate class
                    self.callable = self.callable()
                values = ()
            elif type(values[0]) in (list, tuple):
                if len(values) > 1:
                    self.usage('only one list argument')
                values = values[0]
        self.values = values

    def calc_values(self, available):
        if self.callable:
            return [v for v in available if self.callable(v)]
        return self.values

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__,
                           ', '.join([repr(v) for v in self.values]))


class suppress(CondColArgValues):

    def apply(self, summcondcol):
        if summcondcol.suppress_set is None:
            summcondcol.suppress_set = set()
        values = self.calc_values(summcondcol.inverted)
        summcondcol.suppress_set.update(values)


class retain(CondColArgValues):

    def apply(self, summcondcol):
        if summcondcol.suppress_set is None:
            summcondcol.suppress_set = set(self.keys)
        values = self.calc_values(summcondcol.inverted)
        summcondcol.suppress_set.difference_update(values)


class order(CondColArgValues):

    def apply(self, summcondcol):
        if self.callable:
            summcondcol.key_order.sort(self.callable)
        else:
            summcondcol.key_order.sort()
            if self.values:
                prepend = []
                for v in self.values:
                    try:
                        summcondcol.key_order.remove(v)
                    except ValueError:
                        pass
                    else:
                        prepend.append(v)
                summcondcol.key_order = prepend + summcondcol.key_order


class reversed(order):

    def apply(self, summcondcol):
        order.apply(self, summcondcol)
        summcondcol.key_order.reverse()


class coalesce(CondColArgValues):
    def __init__(self, *values, **kwargs):
        CondColArgValues.__init__(self, *values)
        self.label = kwargs.pop('label', None)
        self.value = kwargs.pop('value', None)
        if kwargs:
            raise TypeError('coalesce: unknown keywords arg(s): %s' % 
                            ', '.join(kwargs.keys()))

    def apply_callable(self, summcondcol):
        for v in summcondcol.inverted.keys():
            agv = self.callable(v)
            if agv != v:
                v_rows = summcondcol.inverted.pop(v)
                agv_rows = summcondcol.inverted.get(agv)
                if agv_rows is not None:
                    v_rows = soomfunc.union(agv_rows, v_rows)
                summcondcol.inverted[agv] = v_rows

    def apply_values(self, summcondcol):
        if self.value is None:
            newvalue = self.values[0]
        else:
            newvalue = self.value
        if newvalue not in self.values and newvalue in summcondcol.inverted:
            raise Error('coalesce target value %d conflicts with an '
                        'existing value' % newvalue)
        valrows = []
        vals = []
        for v in self.values:
            rows = summcondcol.inverted.pop(v, None)
            if rows is not None:
                valrows.append(rows)
                vals.append(v)
        if len(valrows) == 0:
            summcondcol.inverted[newvalue] = []
        elif len(valrows) == 1:
            summcondcol.inverted[newvalue] = valrows[0]
        else:
            summcondcol.inverted[newvalue] = soomfunc.union(*valrows)
        if self.label:
            summcondcol.outtrans[newvalue] = self.label
        else:
            vals = [summcondcol.col.do_format(summcondcol.col.do_outtrans(v))
                    for v in vals]
            summcondcol.outtrans[newvalue] = ', '.join(vals)

    def apply(self, summcondcol):
        summcondcol.use_outtrans = True
        if self.callable:
            self.apply_callable(summcondcol)
        elif self.values:
            self.apply_values(summcondcol)

    def __repr__(self):
        repr = CondColArgValues.__repr__(self)
        if self.label:
            repr = '%s, value=%r, label=%r)' % (repr[:-1], self.value, 
                                                self.label)
        return repr


class BinFunction(object): 
    pass


class DateBinFunction(BinFunction):

    def get_nbins(self, col):
        if not col.is_datetimetype():
            raise Error('%s binning but %r is not a date column' %
                        (self.__class__.__name__, col.name))
        return self.nbins

    def get_outtrans(self, col):
        return self.outtrans


class bin_dayofweek(DateBinFunction):
    nbins = 8           # 1 based
    label = 'Day of Week'
    outtrans = {
        1: 'Mon',
        2: 'Tue',
        3: 'Wed',
        4: 'Thu',
        5: 'Fri',
        6: 'Sat',
        7: 'Sun',
    }
    def bin_fn(self, v):
        return v.iso_week[2]


class bin_weekofyear(DateBinFunction):
    nbins = 53          # 1 based
    outtrans = {}
    label = 'Week of Year'

    def bin_fn(self, v):
        return v.iso_week[1]


class bin_monthofyear(DateBinFunction):
    nbins = 13          # 1 based
    label = 'Month'
    outtrans = {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
       10: 'Oct',
       11: 'Nov',
       12: 'Dec', 
    }

    def bin_fn(self, v):
        return v.month


class bin_year(DateBinFunction):
    nbins = 1        # Unknown
    outtrans = {}
    label = 'Year'

    def bin_fn(self, v):
        return v.year

class bin_yearmonth(DateBinFunction):
    nbins = 1        # Unknown
    outtrans = {}
    label = 'Year+Month'

    def bin_fn(self, v):
        return v.year * 100 + v.month


class bin_yearweek(DateBinFunction):
    nbins = 1        # Unknown
    outtrans = {}
    label = 'Year+Week'

    def bin_fn(self, v):
        return v.year * 100 + v.iso_week[1]


class bin_dayofyear(DateBinFunction):
    nbins = 366         # 1 based
    outtrans = {}
    label = 'Day of Year'

    def bin_fn(self, v):
        return v.day_of_year


class bin_dayofmonth(DateBinFunction):
    nbins = 32          # 1 based
    outtrans = {}
    label = 'Day of Month'

    def bin_fn(self, v):
        return v.day


class bin_hour(DateBinFunction):
    nbins = 24
    outtrans = {}
    label = 'Hour of Day'

    def bin_fn(self, v):
        return v.hour


class bin_time(DateBinFunction):
    nbins = 0
    outtrans = {}
    label = 'Time of Day'

    def get_outtrans(self, col):
        outtrans = {}
        for t in xrange(0, 24 * 60):
            outtrans[t] = '%02d:%02d' % (t / 60, t % 60)
        return outtrans

    def bin_fn(self, v):
        return int(v.abstime) / 60


class bins(CondColArgValues):
    named_bin_fns = {
        'weekofyear': bin_weekofyear,
        'month': bin_monthofyear,
        'dayofweek': bin_dayofweek,
        'weekday': bin_dayofweek,
        'year': bin_year,
        'yearmonth': bin_yearmonth,
        'yearweek': bin_yearweek,
        'dayofyear': bin_dayofyear,
        'dayofmonth': bin_dayofmonth,
        'day': bin_dayofmonth,
        'hour': bin_hour,
        'timeofday': bin_time,
        'time': bin_time,
    }

    def __init__(self, *args, **kwargs):
        values = []
        for a in args:
            if isinstance(a, basestring):
                try:
                    a = self.named_bin_fns[a]
                except KeyError:
                    raise Error('Unknown named binning function: %r' % a)
            values.append(a)
        CondColArgValues.__init__(self, *values)
        self.n = kwargs.pop('n', None)
        if self.n is not None:
            if self.values:
                raise Error('Specify either bin breaks or number of bins, '
                            'not both')
            if self.values < 2:
                raise Error('Specify at least 2 bins')

    def even_breaks(self, col):
        """
        Scans the column values to derive self.n even breaks.
        """
        st = time.time()
        missing = None
        if col.is_discrete():
            data = [v for v, vec in col.inverted.items() if len(vec) > 0]
        else:
            data = col.data
        if type(data) is MA.MaskedArray:
            # This isn't very nice, but it makes a huge difference to speed.
            missing = -sys.maxint
            data = data.filled(missing)
        min = max = None
        for v in data:
            if v != missing:
                if v < min or min is None:
                    min = v
                if v > max or max is None:
                    max = v
        span = (max - min) / float(self.n)
        breaks = [min + span * n for n in xrange(1, self.n)]
        soom.info('Calculated %s breaks in %.2fs, min %s, max %s, span %s' %
                  (self.n, time.time() - st, min, max, span))
        return breaks

    def get_bins(self, bins, nbins):
        for i in xrange(nbins):
            bins.append([])
        return bins

    def discrete_bin(self, col, value_bin, bins):
        _bisect = bisect.bisect                  # make it a local for perf.
        for v, vec in col.inverted.items():
            if v is None:
                bin = -1
            else:
                bin = value_bin(v)
            try:
                bins[bin].append(vec)
            except IndexError:
                self.get_bins(bins, bin)
                bins[bin].append(vec)
        for i, vecs in enumerate(bins):
            n = len(vecs)
            if not n:
                bins[i] = []
            elif n == 1:
                bins[i] = vecs[0]
            else:
                bins[i] = soomfunc.union(*vecs)

    def continuous_bin(self, col, value_bin, bins):
        missing = None
        data = col.data
        if type(data) is MA.MaskedArray:
            # This isn't very nice, but it makes a huge difference to speed.
            missing = -sys.maxint
            data = data.filled(missing)
        for i, v in enumerate(data):
            if v == missing:
                bin = -1
            else:
                bin = value_bin(v)
            try:
                bins[bin].append(i)
            except IndexError:
                self.get_bins(bins, bin)
                bins[bin].append(i)

    def bins_to_inverted(self, bins):
        inverted = {}
        missing_bin = len(bins) - 1
        for v, vec in enumerate(bins):
            if v == missing_bin:
                v = None
            # This makes it much slower... why?
#            inverted[v] = Numeric.array(vec, typecode=Numeric.Int)
            inverted[v] = vec
        return inverted

    def make_outtrans(self, col, inverted, breaks):
        outtrans = {}
        format = col.do_format
        last_bin = len(breaks)
        for v, vec in inverted.iteritems():
            if v == 0:
                outtrans[v] = '< %s' % format(breaks[0])
            elif v == last_bin:
                outtrans[v] = '>= %s' % format(breaks[-1])
            elif v is None:
                outtrans[v] = '--'
            else:
                outtrans[v] = '%s - %s' % (format(breaks[v-1]),
                                           format(breaks[v]))
        return outtrans

    def break_bin(self, summcondcol):
        if self.n is not None:
            breaks = self.even_breaks(summcondcol.col)
        else:
            breaks = sorted(self.values)
        _bisect = bisect.bisect                  # make it a local for perf.
        value_bin = lambda v: _bisect(breaks, v)
        bins = self.get_bins([], len(breaks) + 2)   # + 1 for "missing" bin
        if summcondcol.col.is_discrete():
            self.discrete_bin(summcondcol.col, value_bin, bins)
        else:
            self.continuous_bin(summcondcol.col, value_bin, bins)
        summcondcol.inverted = self.bins_to_inverted(bins)
        summcondcol.outtrans = self.make_outtrans(summcondcol.col, summcondcol.inverted, breaks)

    def fn_bin(self, summcondcol):
        nbins = self.callable.get_nbins(summcondcol.col)
        value_bin = self.callable.bin_fn
        summcondcol.outtrans = self.callable.get_outtrans(summcondcol.col)
        summcondcol.outtrans.setdefault(None, 'N/A')
        bins = self.get_bins([], nbins + 1)         # + 1 for "missing" bin
        if summcondcol.col.is_discrete():
            self.discrete_bin(summcondcol.col, value_bin, bins)
        else:
            self.continuous_bin(summcondcol.col, value_bin, bins)
        summcondcol.inverted = self.bins_to_inverted(bins)
        fn_label = getattr(self.callable, 'label', None)
        if fn_label:
            col_label = summcondcol.col.label or summcondcol.col.name
            summcondcol.label = '%s (%s)' % (col_label, fn_label)

    def apply(self, summcondcol):
        if self.callable:
            return self.fn_bin(summcondcol)
        else:
            return self.break_bin(summcondcol)


class condcol:
    def __init__(self, colname, *args, **kwargs):
        self.colname = colname
        self.args = []
        self.order = None
        self.binning = None
        self.label = kwargs.pop('label', None)
        for arg in args:
            if isinstance(arg, (order, reversed)):
                if self.order is not None:
                    raise Error('only one "order" per condcol allowed')
                self.order = arg
            elif isinstance(arg, bins):
                if self.binning is not None:
                    raise Error('only one "bins" per condcol allowed')
                self.binning = arg
            elif not isinstance(arg, CondColArg):
                raise Error('Unknown condcol() argument: %r' % arg)
            else:
                self.args.append(arg)

    def apply(self, summcondcol):
        if self.binning:
            st = time.time()
            self.binning.apply(summcondcol)
            soom.info("Summarise binning took %.2fs" % (time.time() - st))
        elif summcondcol.col.is_discrete():
            summcondcol.inverted = dict(summcondcol.col.inverted)
            summcondcol.outtrans = dict(summcondcol.col.outtrans)
        else:
            raise Error('%r is not a discrete column' % self.colname)
        for arg in self.args:
            arg.apply(summcondcol)
        summcondcol.key_order = summcondcol.inverted.keys()
        if self.order is None:
            summcondcol.key_order.sort()
        else:
            self.order.apply(summcondcol)

    def __cmp__(self, other):
        if isinstance(other, self.__class__):
            return cmp(self.colname, other.colname)
        # This allows others to compare new-style condcol() args with old-style
        # string column name args (we convert all to condcol internally).
        return cmp(self.colname, other)

    def get_colname(self):
        return self.colname

    def __repr__(self):
        res = []
        res.append(repr(self.colname))
        for arg in self.args:
            res.append(repr(arg))
        return 'condcol(%s)' % (', '.join(res))
