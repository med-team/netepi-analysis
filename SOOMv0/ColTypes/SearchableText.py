# vim: set sw=4 et ai:
#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: SearchableText.py 3690 2009-02-09 05:58:21Z andrewm $
# $Source: /usr/local/cvsroot/NSWDoH/SOOMv0/SOOMv0/ColTypes/SearchableText.py,v $

# Standard Libraries
import os
import re
import struct
import array
from time import time
from bsddb import db

# Application modules
from soomfunc import strip_word

from SOOMv0.common import *
from SOOMv0.Soom import soom
from SOOMv0.ColTypes.base import DatasetColumnBase


ITEMFMT = 'L'

class WordInfo(object):
    __slots__ = ('overflow_blocks', 'occurrences')

    def __init__(self):
        self.overflow_blocks = None
        self.occurrences = array.array(ITEMFMT)


class SearchableTextDatasetColumn(DatasetColumnBase):

    coltype = 'searchabletext'
    loadables = DatasetColumnBase.loadables + ('wordidx', 'wordidx_overflow')
    OVERFLOW_BLOCKSIZE = 4096
    OVERFLOW_ITEMSIZE = array.array(ITEMFMT).itemsize
    OVERFLOW_MAGIC = 0x567eadbe
    OVERFLOW_HDR = '>LLL'

    def is_searchabletext(self):
        return True

    WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)

    def __init__(self, *args, **kw):
        """
        Initialise the cache
        """
        DatasetColumnBase.__init__(self, *args, **kw)
        self._wordidx = None
        self._wordidx_overflow = None
        self._overflow_blocksize = None
        self._overflow_blockitems = None

    def _wordidx_open(self, mode='r'):
        fn = self.object_path('wordidx', 'db', mkdirs=(mode == 'c'))
        worddb = db.DB()
        if mode == 'c':
            worddb.set_cachesize(0, 8<<20)
            worddb.open(fn, db.DB_HASH, db.DB_CREATE, 0666)
        else:
            worddb.open(fn, db.DB_HASH, db.DB_RDONLY)
        return worddb

    def _overflow_open(self, mode='r'):
        fn = self.object_path('wordidx_overflow', 'data', mkdirs=(mode == 'c'))
        if mode == 'c':
            return open(fn, 'wb')
        else:
            return open(fn, 'rb')

    def _overflow_hdr(self, f, count):
        hdr = struct.pack(self.OVERFLOW_HDR, self.OVERFLOW_MAGIC, 
                          self.OVERFLOW_BLOCKSIZE, count)
        hdr += '\0' * (self.OVERFLOW_BLOCKSIZE - len(hdr))
        f.seek(0, 0)
        f.write(hdr)

    def _wordidx_gen(self, src):
        # Keep a record of the location of each occurrence of a word
        self.unload_wordidx()
        self.unload_wordidx_overflow()
        t0 = time()
        worddb = self._wordidx_open('c')
        overflow = self._overflow_open('c')
        self._overflow_hdr(overflow, 0)
        overflow_block = 1
        block_items = self.OVERFLOW_BLOCKSIZE / self.OVERFLOW_ITEMSIZE
        wrecs = {}
        for rownum, value in enumerate(src):
            if value:
                for wordnum, match in enumerate(self.WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    try:
                        wrec = wrecs[word]
                    except KeyError:
                        wrec = wrecs[word] = WordInfo()
                    occurrences = wrec.occurrences
                    occurrences.append(rownum)
                    occurrences.append(wordnum)
                    if len(occurrences) == block_items:
                        if wrec.overflow_blocks is None:
                            wrec.overflow_blocks = array.array(ITEMFMT)
                        occurrences.tofile(overflow)
                        wrec.overflow_blocks.append(overflow_block)
                        overflow_block += 1
                        del occurrences[:]
            yield value
        t1 = time()
        for word, wrec in wrecs.iteritems():
            data = array.array(ITEMFMT)
            if wrec.overflow_blocks is not None:
                data.append(len(wrec.overflow_blocks))
                data.extend(wrec.overflow_blocks)
            else:
                data.append(0)
            data.extend(wrec.occurrences)
            worddb[word] = data.tostring()
        worddb.close()
        self._overflow_hdr(overflow, overflow_block)
        overflow.close()
        t2 = time()
        overflow_size = overflow_block * self.OVERFLOW_BLOCKSIZE
        soom.info('word index for %r took %.1fs (%.1fs+%.1fs), %d words, '
                  '%d overflow blocks (%.1fMB)' % 
                  (self.name, t2-t0, t1-t0, t2-t1, len(wrecs),
                   overflow_block, overflow_size / 1024.0 / 1024.0))

    def get_store_chain(self, data, mask=None):
        src = iter(data)
        if mask is not None:
            src = self._mask_gen(src, mask)
        if self.missingvalues:
            src = self._missing_gen(src, self.missingvalues)
        src = self._storedata_gen(src)
        src = self._wordidx_gen(src)
        return src

#    def _store_wordidx(self):
#        """
#        Make sure the accumulated word information is flushed
#        """
#        if not self.parent_dataset.backed:
#            raise Error('Searchable text requires a "backed" dataset')
#        self.flush()

    def get_wordidx(self):
        if self._wordidx is None:
            self._wordidx = self._wordidx_open()
        return self._wordidx
    wordidx = property(get_wordidx)

    def get_wordidx_overflow(self):
        if self._wordidx_overflow is None:
            self._wordidx_overflow = self._overflow_open()
            hdr_size = struct.calcsize(self.OVERFLOW_HDR)
            hdr = self._wordidx_overflow.read(hdr_size)
            try:
                magic, blocksize, blockcount =\
                    struct.unpack(self.OVERFLOW_HDR, hdr)
            except struct.error, e:
                raise DatasetError('%s: header: %s' % (fn, e))
            if magic != self.OVERFLOW_MAGIC:
                raise DatasetError('%s: incorrect magic' % (fn))
            self._wordidx_overflow.seek(0, 2)
            filesize = self._wordidx_overflow.tell()
            if filesize != blockcount * blocksize:
                raise DatasetError('%s: incorrect file size (expect %s, got %s)'
                                    % (fn, blockcount * blocksize, filesize))
            self._overflow_blocksize = blocksize
            self._overflow_blockitems = blocksize / self.OVERFLOW_ITEMSIZE
        return self._wordidx_overflow
    wordidx_overflow = property(get_wordidx_overflow)

    def unload_wordidx(self):
        if self._wordidx is not None:
            self._wordidx.close()
            self._wordidx = None

    def unload_wordidx_overflow(self):
        if self._wordidx_overflow is not None:
            self._wordidx_overflow.close()
            self._wordidx_overflow = None
            self._overflow_blocksize = None
            self._overflow_blockitems = None

    def op_contains(self, sexpr):
        return sexpr(self)[0]

    def word_occurrences(self, word):
        # first look for the word itself in the index
        try:
            buf = self.wordidx[strip_word(word)]
        except KeyError:
            return None
        info = array.array(ITEMFMT)
        info.fromstring(buf)
        overflow_block_count = info[0]
        overflow_blocks = info[1:overflow_block_count+1]
        residual_occurrences = info[overflow_block_count+1:]
        occurrences = array.array(ITEMFMT)
        for block in overflow_blocks:
            self.wordidx_overflow.seek(block * self._overflow_blocksize, 0)
            occurrences.fromfile(self.wordidx_overflow, 
                                 self._overflow_blockitems)
        occurrences.extend(residual_occurrences)
        return occurrences
