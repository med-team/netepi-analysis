#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: Discrete.py 3693 2009-02-10 05:36:00Z andrewm $
# $Source: /usr/local/cvsroot/NSWDoH/SOOMv0/SOOMv0/ColTypes/Discrete.py,v $

import time
import sets
import operator
import re
try:
    set
except NameError:
    from sets import Set as set

import Numeric, MA
import soomfunc
from soomarray import ArrayDict
from SOOMv0.common import *
from SOOMv0.Soom import soom
from SOOMv0.ColTypes.base import DatasetColumnBase

class _DiscreteDatasetColumn(DatasetColumnBase):
    loadables = ['data', 'inverted']

    def __init__(self, parent_dataset, name, 
                 all_value=None, all_label=None, 
                 **kwargs):
        DatasetColumnBase.__init__(self, parent_dataset, name, **kwargs)
        self._inverted = {}
        if all_label is None:
            all_label = '<All>'
        self.all_label = all_label
        # the datatype of the value used to represent "all" must be
        # consistent with the datatype of the column...
        if all_value is None:
            self.all_value = self.datatype.default_all_value
        else:
            try:
                self.all_value = self.datatype.as_pytype(all_value)
            except (ValueError, TypeError):
                raise Error('The all_value given, %r, for column %s does not match datatype %s' % (all_value, self.name, self.datatype.name))

    def do_outtrans(self, v):
        try:
            if not isinstance(v, MA.MaskedScalar) and v == self.all_value:
                return self.all_label
        except:
            # mx.DateTime can raise mx.DateTime.Error here, sigh.
            pass
        return DatasetColumnBase.do_outtrans(self, v)

    def is_discrete(self):
        return True

    def cardinality(self):
        """Method to report the cardinality of a categorical column"""
        return len(self.inverted)

    def load_inverted(self):
        if self.parent_dataset.backed:
            if self._inverted is None:
                starttime = time.time()
                filename = self.object_path('inverted', 'SOOMblobstore')
                self._inverted = ArrayDict(filename, 'r')
                elapsed = time.time() - starttime
                soom.info('load of %r index took %.3f seconds.' %\
                            (self.name, elapsed))
        else:
            # we need to build the inverted index!
            self._build_inverted()

    def unload_inverted(self):
        self._inverted = None

    def get_inverted(self):
        if self._inverted is None:
            self.load_inverted()
        return self._inverted
    inverted = property(get_inverted)

    def _build_inverted(self):
        """
        Build an inverted index

        NOTE - This is now only used where there is no on-disk
        inverted index, but the column is discrete. For persistent
        discrete columns, the inverted index is built as the data
        is filtered, and the inverted index is saved along with
        the data.
        """
        starttime = time.time() # keep track of time
        inverted_dict = {}
        # Use fast NumPy methods if the column type is numeric
        if self.is_numerictype():
            # first get all the unique values
            uniquevalues = soomfunc.unique(Numeric.sort(self._data.compressed()))
            ordinals = Numeric.array(range(len(self._data)),
                                        typecode=Numeric.Int)
            for value in uniquevalues:
                inverted = Numeric.compress(Numeric.where(Numeric.equal(self._data,value),1,0),ordinals)
                inverted_dict[value] = inverted
        else:
            # loop over each element
            for rownum, value in enumerate(self._data):
                if type(value) is tuple:
                    for v in value:
                        row_nums = inverted_dict.setdefault(v, [])
                        row_nums.append(rownum)
                else:
                    row_nums = inverted_dict.setdefault(value, [])
                    row_nums.append(rownum)
            for value, row_nums in inverted_dict.iteritems():
                row_array = Numeric.array(row_nums, typecode=Numeric.Int)
                if self.datatype.name == 'tuple':
                    row_array = soomfunc.unique(Numeric.sort(row_array))
                inverted_dict[value] = row_array
        self._inverted = inverted_dict
        soom.info('Building inverted index for column %s in dataset %s took %.3f seconds' % (self.name, self.parent_dataset.name, time.time() - starttime))

    def _store_inverted(self, inverted=None):
        """
        Stores the passed inverted index as a memory-mapped dict
        of NumPy ID vectors
        """
        indexfilename = None
        inverted_blob = {}
        if self.parent_dataset.backed:
            indexfilename = self.object_path('inverted', 'SOOMblobstore',
                                             mkdirs=True)
            inverted_blob = ArrayDict(indexfilename, 'w+')
        # now write out the Numpy array for each value in the column to a file
        for value, rownums in inverted.iteritems():
            # TO DO: need to determine the smallest Numpy integer type required
            # to hold all the row ids
            row_array = Numeric.array(rownums, Numeric.Int)
            if self.datatype.name == 'tuple':
                row_array = soomfunc.unique(Numeric.sort(row_array))
            inverted_blob[value] = row_array
        if self.heterosourcecols is not None:
            # we need to assemble an output translation dict
            self.outtrans = {}
            for keytuple in inverted.keys():
                for cname in self.parent_dataset.get_columns():
                    pcol = getattr(self.parent_dataset,cname)
                    if pcol.columnid == keytuple[0]:
                        clabel = pcol.label
                        if callable(pcol.outtrans):
                            cdesc = pcol.outtrans(keytuple[1])
                        else:
                            cdesc = pcol.outtrans[keytuple[1]]
                        newdesc = clabel + ":" + cdesc
                        getattr(self.parent_dataset,colname).outtrans[keytuple] = newdesc
        del inverted                # Not needed anymore
        if indexfilename:
            inverted_blob = None           # Closes and flushes to disk
        self._inverted = inverted_blob

    def _inverted_gen(self, src):
        inverted = {}
        for rownum, value in enumerate(src):
            if type(value) is tuple:
                for v in value:
                    if v is not None or not self.ignorenone:
                        row_nums = inverted.setdefault(v, [])
                        row_nums.append(rownum)
            else:
                try:
                    row_nums = inverted.setdefault(value, [])
                except TypeError, e:
                    raise Error('column %r: Bad value: %r %s: %s' % 
                                (self.name, value, type(value), e))
                row_nums.append(rownum)
            yield value
        self._store_inverted(inverted)

    def get_store_chain(self, data, mask=None):
        src = DatasetColumnBase.get_store_chain(self, data, mask)
        src = self._inverted_gen(src)
        return src

    def describe(self, detail=ALL_DETAIL):
        d = DatasetColumnBase.describe(self, detail)
        if detail >= SOME_DETAIL:       # Don't load .inverted otherwise
            d.add('data', SOME_DETAIL, 'Cardinality', self.cardinality())
        d.add('data', SOME_DETAIL, 'Label for <All>', self.all_label)
        d.add('data', SOME_DETAIL, 'Value for <All>', str(self.all_value))
        return d

    def _op_general(self, fn, discardnull=True):
        # handle the general case for an operator combining vectors of results
        # for each operator.
        possible_keys = set(self.inverted.keys())
        if discardnull:
            possible_keys.discard(None)
        rows = [self.inverted[v] for v in possible_keys if fn(v)]
        if len(rows) == 1:
            vectors = rows[0]
        elif len(rows) > 1:
            vectors = soomfunc.union(*rows)
        else:
            vectors = []
        return vectors

    def op_between(self, value):
        try:
            start, end = value
        except (ValueError, TypeError):
            raise ExpressionError('between(start, end)')
        possible_keys = set(self.inverted.keys())
        rows = [self.inverted[v] 
                for v in possible_keys 
                if start <= v < end]
        if len(rows) == 0:
            vectors = []
        elif len(rows) == 1:
            vectors = rows[0]
        else:
            vectors = soomfunc.union(*rows)
        return vectors

    def op_equal(self, value):
        # special case for operator equal as we can just do a direct lookup of
        # the inverted index.
        return self.inverted.get(value, [])

    def op_less_than(self, value):
        return self._op_general(lambda v: v < value)

    def op_less_equal(self, value):
        return self._op_general(lambda v: v <= value)

    def op_greater_than(self, value):
        return self._op_general(lambda v: v > value)

    def op_greater_equal(self, value):
        return self._op_general(lambda v: v >= value)

    def op_not_equal(self, value):
        return self._op_general(lambda v: v != value, discardnull=False)

    def op_equal_col(self, value):
        return self._op_general(self.prefix_match(operator.eq, value))

    def op_not_equal_col(self, value):
        return self._op_general(self.prefix_match(operator.ne, value))

    def op_less_than_col(self, value):
        return self._op_general(self.prefix_match(operator.lt, value))

    def op_less_equal_col(self, value):
        return self._op_general(self.prefix_match(operator.le, value))

    def op_greater_than_col(self, value):
        return self._op_general(self.prefix_match(operator.gt, value))

    def op_greater_equal_col(self, value):
        return self._op_general(self.prefix_match(operator.ge, value))

    def value_set(self, value):
        if type(value) not in (list, tuple):
            raise ExpressionError('"in" operator must be followed by a list')
        return set(value)

    def op_in(self, value):
        value = self.value_set(value)
        return self._op_general(lambda v: v in value, discardnull=False)

    def op_not_in(self, value):
        value = self.value_set(value)
        return self._op_general(lambda v: v not in value, discardnull=False)

    def op_in_col(self, value):
        return self._op_general(self.prefix_match_in(value))

    def op_not_in_col(self, value):
        match_fn = self.prefix_match_in(value)
        return self._op_general(lambda v: not match_fn(v))

    def op_regexp(self, value):
        return self._op_general(self.regexp_match(value))

    def op_not_regexp(self, value):
        match_fn = self.regexp_match(value)
        return self._op_general(lambda v: not match_fn(v))

    def regexp_match(self, value):
        pat = re.compile(value, re.IGNORECASE)
        def _op_regexp(v):
            return pat.search(self.do_format(self.do_outtrans(v)))
        return _op_regexp
        
    def prefix_match(self, match_fn, value):
        value = self.do_format(value)
        value_len = len(value)
        def _op_prefix(v):
            return match_fn(self.do_format(self.do_outtrans(v))[:value_len], value)
        return _op_prefix

    def prefix_match_in(self, value):
        if type(value) not in (list, tuple):
            raise ExpressionError('"in" operator must be followed by a list')
        values = [self.do_format(v) for v in value]
        def _op_prefix(v):
            v = self.do_format(self.do_outtrans(v))
            for value in values:
                if v.startswith(value):
                    return True
            return False
        return _op_prefix


class CategoricalDatasetColumn(_DiscreteDatasetColumn):
    coltype = 'categorical'


class OrdinalDatasetColumn(_DiscreteDatasetColumn):
    coltype = 'ordinal'

    def is_ordered(self):
        return True
