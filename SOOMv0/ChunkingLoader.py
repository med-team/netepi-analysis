#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# $Id: ChunkingLoader.py 2901 2007-11-20 04:52:21Z andrewm $
# $Source: /usr/local/cvsroot/NSWDoH/SOOMv0/SOOMv0/ChunkingLoader.py,v $

import os
import zlib
import cPickle
from time import time
from SOOMv0.Soom import soom

class ChunkingLoader:
    """
    "Rotate" row-wise data to column-wise.

    Do rotation on disk if number of rows is excessive
    """
    compress_chunk = True
    chunk_ext = 'SOOMchunk'

    def __init__(self, columns, basepath):
        self.basepath = basepath
        self.columns = []
        self._init()
        for col in columns:
            if col.name != 'row_ordinal':               # XXX
                self.columns.append((col, []))

    def _chunk_filename(self, colname, chunknum):
        return os.path.join(self.basepath, 
                        '%s.%s.%s' % (colname, chunknum, self.chunk_ext))

    def _init(self):
        self.numchunks = 0
        self.rownum = 0
        self.chunklen = 0
        self.flushtime = 0.0
        self.sourcetime = 0.0
        self.nsources = 0

    def clear(self):
        for fn in os.listdir(self.basepath):
            if fn.endswith(self.chunk_ext):
                os.unlink(os.path.join(self.basepath, fn))

    def flush(self):
        t0 = time()
        for col, data in self.columns:
            fn = self._chunk_filename(col.name, self.numchunks)
            f = open(fn, 'wb')
            try:
                if self.compress_chunk:
                    f.write(zlib.compress(cPickle.dumps(data, -1)))
                else:
                    cPickle.dump(data, f, -1)
            finally:
                f.close()
            del data[:]
        self.chunklen = 0
        self.numchunks += 1
        soom.mem_report()
        flush_el = time() - t0
        self.flushtime += flush_el
        return flush_el

    def get_chunk(self, colname, chunknum):
        filename = self._chunk_filename(colname, chunknum)
        f = open(filename, 'rb')
        try:
            if self.compress_chunk:
                return cPickle.loads(zlib.decompress(f.read()))
            else:
                return cPickle.load(f)
        finally:
            f.close()

    def loadrows(self, sourcename, source, chunkrows=0, rowlimit=0):
        source_rownum = 0
        t0 = t1 = time()
        initial_flushtime = self.flushtime
        if not rowlimit:
            rowlimit = -1
        self.nsources += 1
        for row in source:
            source_rownum += 1
            for col, data in self.columns:
                data.append(row.get(col.name, None))
            self.rownum += 1
            self.chunklen += 1
            if source_rownum == rowlimit:
                break
            if chunkrows and self.chunklen >= chunkrows:
                el = self.flush()
                t1 += el        # Credit flushtime
            if source_rownum and source_rownum % 1000 == 0:
                t2 = time() 
                el = t2 - t1
                t1 = t2
                soom.info('%s rows from source %r (%.1f rows/s)' %
                          (source_rownum, sourcename, 1000 / el))
        if self.chunklen:
            self.flush()
        flushtime = self.flushtime - initial_flushtime
        sourcetime = time() - t0 - flushtime
        self.sourcetime += sourcetime
        rps = 0.0
        if sourcetime:
            rps = source_rownum / sourcetime
        soom.info('%s rows from source %r in %.1fs '
                  '(%.1fs source, %.1fs chunking, %.1f rows/s)' %
                  (source_rownum, sourcename, 
                   flushtime + sourcetime, sourcetime, flushtime, rps))
        return source_rownum

    def load_completed(self):
        rps = 0.0
        if self.sourcetime:
            rps = self.rownum / self.sourcetime
        soom.info('%s rows from %s source(s) in %.1fs '
                  '(%.1fs source, %.1fs chunking, %.1f rows/s)' %
                  (self.rownum, self.nsources, 
                   self.flushtime + self.sourcetime, 
                   self.sourcetime, self.flushtime, 
                   rps))
        return self.rownum

    def unchunk_columns(self):
        def _col_generator(self, col):
            for chunknum in xrange(self.numchunks):
                data = self.get_chunk(col.name, chunknum)
                os.remove(self._chunk_filename(col.name, chunknum))
                for v in data:
                    yield v

        for col, data in self.columns:
            yield col, _col_generator(self, col)
