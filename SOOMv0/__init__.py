#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#
# SOOMv0.py
# SOOM Proof-of-concept implementation Version 0
# Written by Tim Churches April-November 2001
# Substantially revised by Tim Churches and Ben Golding, April-May 2003
# Extensive further work by Tim Churches and Andrew McNamara, May 2003-...

# $Id: __init__.py 2859 2007-10-18 07:45:37Z andrewm $

#import sys
#interactive = hasattr(sys, 'ps1')
#del sys

# Module imports
from SOOMv0.Soom import soom
from SOOMv0.DataSourceColumn import DataSourceColumn
from SOOMv0.Dataset import Dataset
from SOOMv0.SummaryCond import *
from SOOMv0.SummaryStats import *
from SOOMv0.SummaryProp import propn_names_and_labels, extract_propn_cols
from SOOMv0.Filter import filtered_ds, sampled_ds, sliced_ds
from SOOMv0.Datasets import Datasets
from SOOMv0.DataTypes import datatypes
from SOOMv0.PlotRegistry import plot
from SOOMv0.common import *             # Exceptions, constants, etc
import SOOMv0.interactive_hook          # Interactive friendliness
#from SOOMv0.TransformFN import *

#try:
#    import psyco
#except ImportError:
#    pass
#else:
#    psyco.log('/tmp/soompsyco.log', 'a')
#    psyco.bind(Dataset)
#    psyco.bind(DatasetColumn)
#    psyco.bind(DatasetFilter)
##    psyco.full()

datasets = Datasets()
dsload = datasets.dsload
dsunload = datasets.dsunload
makedataset = datasets.makedataset
subset = datasets.subset
