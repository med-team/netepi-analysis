#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#

"""
This script parses the output produced when loading a SOOM dataset in
verbose mode and produces a summary report to aid performance tuning.
"""

import re
import fileinput

class Column:
    maxname = 0

    def __init__(self, name):
        self.derived = False
        self.name = name
        self.totaltime = 0.0
        self.times = {}
        self.mem = None
        if len(name) > Column.maxname:
            Column.maxname = len(name)

    def add_time(self, name, elap):
        self.times[name] = self.times.get(name, 0.0) + elap
        self.totaltime += elap

    @classmethod
    def hdr(cls):
        print
        print '%-*s %8s %13s' %\
            (cls.maxname, 'Column', 'mem', 'tot time  ')
        print '-' * 79

    def report(self):
        rep = '%-*s %8s %13s' %\
            (self.maxname, self.name, 
             kmeg(self.mem),
             elap(self.totaltime, ralign=True))
        extra = []
        for key in sorted(self.times):
            elap_time = self.times[key]
            extra.append(' %s: %s' % (key, elap(elap_time)))
        rep += ','.join(extra)
        print rep


class MatchMeta(type):
    def __init__(cls, name, bases, dict):
        if hasattr(cls, 're'):
            Match.matches.append(cls())

class Match:
    __metaclass__ = MatchMeta
    matches = []

    def __init__(self):
        self.cre = re.compile(self.re)

    def match(self, state, line):
        match = self.cre.search(line)
        if match:
            self.action(state, match)

    def kill(self, cls):
        Match.matches = [match for match in self.matches
                         if match.__class__ is not cls]

    def set_column(self, state, name):
        if state.cur_column is None or state.cur_column.name != name:
            col = Column(name)
            state.cur_column = col
            state.columns.append(col)

    @classmethod
    def run(self, state, line):
        for match in self.matches:
            match.match(state, line)


class DSNAME(Match):
    re = r"Dataset '([^']+)' loaded"

    def action(self, state, match):
        state.dsname = match.group(1)
        self.kill(DSNAME)

class ChunkFlush(Match):
    re = r'chunk flush took ([\d.]+) seconds'
    def action(self, state, match):
        t = float(match.group(1))
        state.chunkflush += t
        state.chunkflush_count += 1
        if state.chunkflush_min is None or t < state.chunkflush_min:
            state.chunkflush_min = t
        if state.chunkflush_max is None or t > state.chunkflush_max:
            state.chunkflush_max = t

class Mem(Match):
    re = r'mem delta: [\d-]+k, total: (\d+)k'
    def action(self, state, match):
        mem = int(match.group(1))
        if state.mem_initial is None:
            state.mem_initial = mem
        if state.cur_column is not None and state.mem_final:
            state.cur_column.mem = mem - state.mem_final
        state.mem_final = mem
        if mem > state.mem_peak:
            state.mem_peak = mem

class EndChunking(Match):
    re = r'(\d+) rows read from DataSource (\S+), in ([\d.]+) seconds \((\d+) rows total\)'

    def action(self, state, match):
        self.kill(ChunkFlush)
        state.mem_chunk = state.mem_final
        state.rowcount = int(match.group(1))
        state.srcname = match.group(2)
        state.loadtime = float(match.group(3))
        state.times.append((state.loadtime, 'chunking & loading'))


class StoredCol(Match):
    re = r'Stored data for column (\S+) in dataset \S+ \(([\d.]+)s\)'

    def action(self, state, match):
        storetime = float(match.group(2))
        self.set_column(state, match.group(1))
        storetime -= state.cur_column.times.get('wordidx', 0)
        state.cur_column.add_time('store', storetime)
        state.times.append((storetime, 'store %s' % state.cur_column.name))

class DerivedCol(Match):
    re = r'Creating derived column (\S+) in dataset \S+ took ([\d.]+), store took ([\d.]+)'

    def action(self, state, match):
        assert state.cur_column.name == match.group(1)
        state.cur_column.derived = True
        dertime = float(match.group(2))
        state.cur_column.add_time('derive', dertime)
        state.times.append((dertime, 'derive %s' % state.cur_column.name))

class WordIndexCol(Match):
    re = r"word index for '(\S+)' took ([\d.]+)s \([\d.]+s\+[\d.]+s\), \d+ words, \d+ overflow blocks \([\d.]+MB\)"

    def action(self, state, match):
        self.set_column(state, match.group(1))
        wordtime = float(match.group(2))
        state.cur_column.add_time('wordidx', wordtime)
        state.times.append((wordtime, 'word index %s' % state.cur_column.name))


class State:
    dsname = None
    srcname = None
    chunkflush = 0.0
    chunkflush_min = None
    chunkflush_max = None
    chunkflush_count = 0
    rowcount = None
    loadtime = None
    mem_initial = None
    mem_chunk = None
    mem_final = None
    mem_peak = 0
    cur_column = None
    
    def __init__(self):
        self.columns = []
        self.times = []

def elap(sec, ralign=False):
    if sec is None:
        ret = 'None'
    elif sec < 10:
        ret = '%.1fs' % sec
    elif sec < 90:
        ret = '%.0fs  ' % sec
    else:
        min, sec = divmod(sec, 60)
        if min < 90:
            ret = '%dm %2ds  ' % (min, sec)
        else:
            hr, min = divmod(min, 60)
            ret = '%dh %2dm      ' % (hr, min)
    if ralign:
        return ret
    return ret.rstrip()

def kmeg(h, l=0):
    if h is None or l is None:
        return '?'
    return '%.1fM' % ((h - l) / 1024.0)

def report(state):
    print 'Dataset: %s' % state.dsname
    print 'Chunk flushing: %s, total: %s (chunk min %s, max %s, %d chunks)' %\
        (elap(state.chunkflush), 
         elap(state.loadtime), 
         elap(state.chunkflush_min),
         elap(state.chunkflush_max),
         state.chunkflush_count)
    print 'mem total: %s, chunk: %s, peak %s' %\
        (kmeg(state.mem_final, state.mem_initial),
         kmeg(state.mem_chunk, state.mem_initial),
         kmeg(state.mem_peak, state.mem_initial))
    Column.hdr()
    for col in state.columns:
        col.report()
    print
    print 'Top 10 time consumers:'
    times = sorted(state.times)
    for t, name in times[-1:-11:-1]:
        print '  %13s %s' % (elap(t, ralign=True), name)
        


def main():
    dsname = None
    state = State()
    for line in fileinput.input():
        Match.run(state, line)
    report(state)

if __name__ == '__main__':
    main()
