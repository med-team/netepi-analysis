# vim: set sw=4 et ai:
#
#   The contents of this file are subject to the HACOS License Version 1.2
#   (the "License"); you may not use this file except in compliance with
#   the License.  Software distributed under the License is distributed
#   on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the LICENSE file for the specific language governing
#   rights and limitations under the License.  The Original Software
#   is "NetEpi Analysis". The Initial Developer of the Original
#   Software is the Health Administration Corporation, incorporated in
#   the State of New South Wales, Australia.
#
#   Copyright (C) 2004,2005 Health Administration Corporation. 
#   All Rights Reserved.
#

"""
Code for experimenting with and benchmarking techniques for indexing
the SearchableText columns.
"""

import os
import sys
import gzip
import re
import array
from time import time
from bsddb import db

from soomfunc import strip_word
from soomarray import ArrayVocab

import SOOMv0
from SOOMv0.ColTypes.SearchableText import Occurrences

def timeit(f, *a, **kw):
    st = time()
    ret = f(*a, **kw)
    el = time() - st
    print '%s %.2fs (%.1f minutes)' % (f.__name__, el, el / 60)
    return ret

#------------------------------------------------------------------------------
# Just time iteration over the rows
if 0:
    def read(f):
        for rownum, value in enumerate(f):
            pass

    f = gzip.open(sys.argv[1])
    timeit(read, f)

#------------------------------------------------------------------------------
# Iterate over the rows, splitting into words, and iterating over those
if 0:
    def read_and_split(f):
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        cnt = 0
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    cnt += 1
        print cnt

    f = gzip.open(sys.argv[1])
    timeit(read_and_split, f)

#------------------------------------------------------------------------------
# As above, but record word indexes within a record
if 0:
    def read_split_and_line_index(f):
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                words = {}
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    words.setdefault(word, []).append(wordnum)

    f = gzip.open(sys.argv[1])
    timeit(read_split_and_line_index, f)

#------------------------------------------------------------------------------
# Iterate over rows, splitting into words, assign unique word number
if 0:
    def read_split_and_word_index(f):
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        words = {}
        next_word_num = 0
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    try:
                        word_num = words[word]
                    except KeyError:
                        word_num = words[word] = next_word_num
                        next_word_num += 1
        return words

    f = gzip.open(sys.argv[1])
    words = timeit(read_split_and_word_index, f)

#------------------------------------------------------------------------------
# Iterate rows and words, recording unique words in the Occurrences abstraction
# (the result is conceptually like the previous test, so we get to see how much
# impact Occurrences has).
if 0:
    def index_occurrences(f):
        SOOMv0.soom.messages = 1
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        word_first_last = {}
        occurrences = Occurrences('occurrences', 'c')
        next_word_num = 0
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                words = {}
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    words.setdefault(word, []).append(wordnum)
                for word, wordnums in words.iteritems():
                    first_last = word_first_last.get(word, None)
                    new_first_last = occurrences.add_row_wordnums(first_last, rownum, wordnums)
                    if first_last != new_first_last:
                        word_first_last[word] = new_first_last
            if rownum % 1000 == 0:
                occurrences.age()
        occurrences.close()
        return words

    f = gzip.open(sys.argv[1])
    words = timeit(index_occurrences, f)

#------------------------------------------------------------------------------
# Iterate rows and words, inserting unique words into a bsddb hash file
# (aka SearchableText's vocab dictionary).
if 0:
    def index_vocab(f):
        SOOMv0.soom.messages = 1
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        word_first_last = ArrayVocab('vocab', 'c')
    #    occurrences = Occurrences('occurrences', 'c')
        next_word_num = 0
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                words = {}
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    words.setdefault(word, []).append(wordnum)
                for word, wordnums in words.iteritems():
                    first_last = word_first_last.get(word, None)
                    if first_last is None:
                        word_first_last[word] = 0, 0
        return words

    f = gzip.open(sys.argv[1])
    words = timeit(index_vocab, f)

#------------------------------------------------------------------------------
# Iterate rows and words, creating a Word object for each unique word to test
# the viability of a more structured Vocab implementation.
if 0:
    class Word(object):
        __slots__ = ('start','indexes')
        def __init__(self):
            self.start = None
            self.indexes = array.array('L')

    def read_obj(f):
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        cnt = 0
        words = {}
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    try:
                        wrec = words[word]
                    except KeyError:
                        wrec = words[word] = Word()
                    wrec.indexes.append(rownum)
                    wrec.indexes.append(wordnum)
        return words

    f = gzip.open(sys.argv[1])
    words = timeit(read_obj, f)

#------------------------------------------------------------------------------
# Iterate rows and words, recording word positions in the WordStore object.
# When a WordStore instance has more than 500 positions, spill the positions to
# the backing bsddb and clear the array. Finally, flush any WordStores with
# residual positions.
#
# This technique is way too slow - the continual growing of bsddb values is a
# killer for the commonly seen words. After several hours run time, I killed my
# test fun because it was eating my machine alive, and the data file was only a
# few hundred meg.

if 0:
    class WordStore(object):
        __slots__ = ('start','indexes')
        def __init__(self):
            self.start = None
            self.indexes = array.array('L')

    def read_obj_store(f):
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        cnt = 0
        try:
            os.unlink('vocab')
        except OSError:
            pass
        worddb = db.DB()
        worddb.set_cachesize(0, 8<<20)
        worddb.open('vocab', db.DB_HASH, db.DB_CREATE, 0666)
        words = {}
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    try:
                        wrec = words[word]
                    except KeyError:
                        wrec = words[word] = WordStore()
                    indexes = wrec.indexes
                    indexes.append(rownum)
                    indexes.append(wordnum)
                    if len(indexes) == 1024:
                        worddb[word] = worddb.get(word, '') + indexes.tostring()
                        del indexes[:]
                        print "flush %r, %d bytes" % (word, len(worddb[word]))
        print "done, now flushing residual"
        for word, wrec in words.iteritems():
            indexes = wrec.indexes
            if indexes:
                worddb[word] = worddb.get(word, '') + indexes.tostring()
                del indexes[:]
        worddb.close()
        return words

    f = gzip.open(sys.argv[1])
    words = timeit(read_obj_store, f)

#------------------------------------------------------------------------------
# As above, but spill to a separate data file.

if 0:
    class WordStore(object):
        __slots__ = ('occurrences','indexes')
        def __init__(self):
            self.occurrences = array.array('L')
            self.indexes = array.array('L')

    def read_obj_store_sep(f):
        WORD_RE = re.compile(r"[A-Z0-9][A-Z0-9']+", re.I)
        cnt = 0
        try:
            os.unlink('vocab')
        except OSError:
            pass
        worddb = db.DB()
        worddb.set_cachesize(0, 8<<20)
        worddb.open('vocab', db.DB_HASH, db.DB_CREATE, 0666)
        occ = open('occurrences', 'wb')
        occ_count = 0
        words = {}
        for rownum, value in enumerate(f):
            value = value[:-1]
            if value:
                for wordnum, match in enumerate(WORD_RE.finditer(value)):
                    word = strip_word(match.group())
                    try:
                        wrec = words[word]
                    except KeyError:
                        wrec = words[word] = WordStore()
                    indexes = wrec.indexes
                    indexes.append(rownum)
                    indexes.append(wordnum)
                    if len(indexes) == 1024:
                        if wrec.occurrences is None:
                            wrec.occurrences = array.array('L')
                        indexes.tofile(occ)
                        wrec.occurrences.append(occ_count)
                        occ_count += 1
                        del indexes[:]
#                        print "flush %r, %d bytes" % (word, len(worddb[word]))
        print "done, %d spills, now flushing residual" % (occ_count)
        for word, wrec in words.iteritems():
            if wrec.indexes or wrec.occurrences is not None:
                data = array.array('L')
                if wrec.occurrences is not None:
                    data.append(len(wrec.occurrences))
                    data.extend(wrec.occurrences)
                else:
                    data.append(0)
                data.extend(wrec.indexes)
                worddb[word] = data.tostring()
        worddb.close()
        return words

    f = gzip.open(sys.argv[1])
    words = timeit(read_obj_store_sep, f)


#------------------------------------------------------------------------------
# Read data created by the above
if 1:
    class Occurrences:
        def __init__(self):
            self.worddb = db.DB()
            self.worddb.open('vocab', db.DB_HASH, db.DB_RDONLY)
            self.occ = open('occurrences', 'rb')

        def find(self, word):
            data = array.array('L')
            data.fromstring(self.worddb[word.upper()])
            nspills = data[0]
            spillblocks = data[1:nspills+1]
            residual = data[nspills+1:]
            print "found %r, nspills %s, residual len %s" %\
                (word, nspills, len(residual))
            pairs = array.array('L')
            for block in spillblocks:
                self.occ.seek(block * 4096, 0)
                pairs.fromfile(self.occ, 1024)
            pairs.extend(residual)
            return pairs

    occ = Occurrences()

