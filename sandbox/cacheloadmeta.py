from time import time
import gzip
import cPickle
import csv

t0 = time()
m = cPickle.load(gzip.open('SOOM_objects/edis/load_cache/metadata.pkl.gz', 'rb'))
print 'load', time() - t0

if 0:
    t0 = time()
    f = open('/tmp/clm-a', 'w')
    w = csv.writer(f)
    w.writerows(m.key_to_recno.iteritems())
    f.close()
    print 'csv', time() - t0
        
if 0:
    t0 = time()
    f = gzip.open('/tmp/clm-b', 'wb')
    w = csv.writer(f)
    w.writerows(m.key_to_recno.iteritems())
    f.close()
    print 'csv gzip', time() - t0

if 0:
    t0 = time()
    f = gzip.open('/tmp/clm-b', 'rb')
    map = {}
    for key, index in csv.reader(f):
        map[key] = int(index)
    f.close()
    print 'csv gzip load', time() - t0

if 1:
    t0 = time()
    f = open('/tmp/clm-c', 'wb')
    cPickle.dump(m, f, -1)
    f.close()
    print 'pickle dump', time() - t0

if 1:
    t0 = time()
    f = open('/tmp/clm-c', 'rb')
    m2 = cPickle.load(f)
    f.close()
    print 'pickle load', time() - t0




