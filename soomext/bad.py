from Numeric import arrayrange
from soomfunc import union
from mytime import timeit

n = 1000
bad = [arrayrange(i, 40000000, n * 4) for i in xrange(n)]
_ = timeit(union,*bad)
